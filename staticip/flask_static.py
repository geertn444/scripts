#! /usr/bin/python2.7
# -*- coding:utf-8 -*-

import os
from flask import Flask, jsonify
from flask import abort
from flask import make_response
from flask import request
from flask import render_template
from flask import flash
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
from netaddr import *
import socket

#own modules
import mypwd
import my_infoblox_lib as infoblox


username = "gyn94629"
password = mypwd.pwd

#initialize infoblox object
gridmaster = infoblox.blox('10.102.79.18',username,password)


#used to detect NTLM username, doesn't work yet
class RemoteUserMiddleware(object):
    def __init__(self, app):
        self.app = app
    def __call__(self, environ, start_response):
        user = environ.pop('HTTP_X_PROXY_REMOTE_USER', None)
        environ['REMOTE_USER'] = user
        return self.app(environ, start_response)

app = Flask(__name__)

app.config.from_object(__name__)
app.config.update(dict(
    DEBUG = False,
    THREADED = True,
    SECRET_KEY = os.urandom(24)
))

app.wsgi_app = RemoteUserMiddleware(app.wsgi_app)





class IPDetective(Form):
    name = TextField('IP Address:', validators=[validators.required()])

class ReusableForm(Form):
    name = TextField('Name:', validators=[validators.required()])
    
class JustButton(Form):
    x = 1

@app.route("/", methods=['GET', 'POST'])
def root():
    form = JustButton(request.form)
     
    
    print form.errors
    ipaddr = str(request.remote_addr)
    ip_object = IPNetwork(ipaddr + "/24")
    nextip = gridmaster.get_next_available_ip(str(ip_object.network) + "/24")
    #flash(str(ipaddr))
    if request.method == 'POST':
        #name=request.form['name']
        #print name
 
        if form.validate():
            # Save the comment here.
            flash('Hello ')
        else:
            flash('All the form fields are required. ','error')
 
    return render_template('go.html', form=form,iptext=ipaddr,net=ip_object.network,nextip=nextip)

    
 
@app.route("/form", methods=['GET', 'POST'])
def hello():
    form = ReusableForm(request.form)
     
    
    print form.errors
    if request.method == 'POST':
        name=request.form['name']
        print name
 
        if form.validate():
            # Save the comment here.
            flash('Hello ' + name)
        else:
            flash('Error: All the form fields are required. ')
 
    return render_template('hello.html', form=form)

    
@app.route("/ip", methods=['GET', 'POST'])
def IP():
    form = IPDetective(request.form)
     
    
    print form.errors
    if request.method == 'POST':
        name=request.form['name']
        print name
        result = gridmaster.get_ip_info_extattrs(str(name))
        try:
         owner = result['Owner']['value']
        except:
         owner = "Not set"
        try:
          dnsname = str(socket.gethostbyaddr(name)[0])
        except:
          dnsname = "Failed"
        if form.validate():
            # Save the comment here.
            
            flash("Status: " + gridmaster.get_ip_info(name)['status'])
            flash("Reverse DNS Name: " + dnsname)
            flash("Owner: " + owner)
            flash("In DHCP Scope: " + str(gridmaster.in_dhcp_scope(name)))
            flash("In Reserved Range: " + str(gridmaster.in_reserved_range(name)))
            flash("Site: " + result['Site']['value'])
            flash("Sitecode: " + result['SiteCode']['value'])
        else:
            flash('Error: All the form fields are required. ')
 
    return render_template('result.html', form=form)

    
    
    
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=9000)
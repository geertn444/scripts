#Custom Infoblox Module
#Geert Nijs - 2016

#public modules
import json
import requests


class blox:
 def __init__(self,ip,username,pwd):
   self.ip = str(ip)
   self.username = str(username)
   self.password = str(pwd)
   self.baseurl = 'https://' + self.ip + '/wapi/v1.2.1/'
   self.my_headers = {'content-type': 'application/json-rpc'} 

 def get_subnet_info(self,subnet):
  #returns JSON subnet info
 
  url = self.baseurl + 'network?network=' + subnet
  #"https://ibgm.bio.corpnet1.com/wapi/v1.2.1/network?network=159.114.115.0/24&_return_fields=comment"
 
  #disables logging of invalid SSL cert - NOT ENCOURAGED !!
  requests.packages.urllib3.disable_warnings()
  response = requests.get(url, headers=self.my_headers,  auth=(self.username, self.password),verify=False)
  #DEBUG: need to check for success !!!
 
  return response.json()[0]
  
 def get_subnet_dhcp_ips(self,subnet):
  #returns a list of ips inside a subnet that are defined as DHCP in the subnet
 
  url = self.baseurl + 'ipv4address?network=' + subnet + '&types=''DHCP_RANGE'''
  #print url
  #disables logging of invalid SSL cert - NOT ENCOURAGED !!
  requests.packages.urllib3.disable_warnings()
  response = requests.get(url, headers=self.my_headers,  auth=(self.username, self.password),verify=False)
  #DEBUG: need to check for success !!!
  jsonobj = response.json()
  lijst = []
  for entry in jsonobj:
   lijst.append(entry['ip_address'])
  return lijst
  
 def get_mac_info(self,mac):
  return 1
  
 def get_ip_info_extattrs(self,ip):
  #url = "https://ibgm.bio.corpnet1.com/wapi/v1.2.1/ipv4address?ip_address=" + ip + "&_return_fields=extattrs" 
  
  url = self.baseurl + 'ipv4address?ip_address=' + ip + '&_return_fields=extattrs'
   
  #disables logging of invalid SSL cert - NOT ENCOURAGED !!
  requests.packages.urllib3.disable_warnings()
  response = requests.get(url, headers=self.my_headers,  auth=(self.username, self.password),verify=False)
  #DEBUG: need to check for success !!!
  
  try:
   r = response.json()[0]['extattrs']
  except:
   r = "Nothing returned"
    
  #Return Dictionary
  return r
  
 def get_ip_info(self,ip):
  #url = "https://ibgm.bio.corpnet1.com/wapi/v1.2.1/ipv4address?ip_address=" + ip + "&_return_fields=extattrs"
 
  url = self.baseurl + 'ipv4address?ip_address=' + ip 
   
  #disables logging of invalid SSL cert - NOT ENCOURAGED !!
  requests.packages.urllib3.disable_warnings()
  response = requests.get(url, headers=self.my_headers,  auth=(self.username, self.password),verify=False)
  #DEBUG: need to check for success !!!
  
  try:
   r = response.json()[0]
  except:
   r = "Nothing returned"
    
  #Return Dictionary  
  return r
  
 def in_dhcp_scope(self,ip):
  #returns 1 if ip is part of a DHCP scope, 0 if it is not
  
  url = self.baseurl + 'ipv4address?ip_address=' + ip 
   
  #disables logging of invalid SSL cert - NOT ENCOURAGED !!
  requests.packages.urllib3.disable_warnings()
  response = requests.get(url, headers=self.my_headers,  auth=(self.username, self.password),verify=False)
  #DEBUG: need to check for success !!!
  
  return 'DHCP_RANGE' in response.json()[0]['types']
 
 def in_reserved_range(self,ip):
  #returns 1 if ip is part of a RESERVED RANGE, 0 if it is not
  
  url = self.baseurl + 'ipv4address?ip_address=' + ip 
   
  #disables logging of invalid SSL cert - NOT ENCOURAGED !!
  requests.packages.urllib3.disable_warnings()
  response = requests.get(url, headers=self.my_headers,  auth=(self.username, self.password),verify=False)
  #DEBUG: need to check for success !!!
  
  return 'RESERVED_RANGE' in response.json()[0]['types']
  
 def get_next_available_ip(self, network):
  url = self.baseurl + 'network?network=' + network
  print url
  print
  #disables logging of invalid SSL cert - NOT ENCOURAGED !!
  requests.packages.urllib3.disable_warnings()
  response = requests.get(url, headers=self.my_headers,  auth=(self.username, self.password),verify=False)
  #DEBUG: need to check for success !!!
  r_json = response.json()
  print r_json
  print
  try:
   net_ref = r_json[0]['_ref']
  except IndexError:
   net_ref = "None returned"
   return "None returned"
  
  
  url = self.baseurl + net_ref + '?_function=next_available_ip&num=1'
  print url
  #disables logging of invalid SSL cert - NOT ENCOURAGED !!
  requests.packages.urllib3.disable_warnings()
  response = requests.post(url, headers=self.my_headers,  auth=(self.username, self.password),verify=False)
  #DEBUG: need to check for success !!!
  
  r_json = response.json()
  print r_json
  ip_v4 = r_json['ips'][0]
  
  return ip_v4
  
  
 
 
 
  
 
 



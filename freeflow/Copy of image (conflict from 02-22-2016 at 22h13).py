#Experiments with Image Recognition

import numpy as np
import cv2

print cv2.__version__

image = cv2.imread("Screenshot.png")
output = image.copy()
image = cv2.medianBlur(image,5)

hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

#filter for DARK BLUE
# define range of blue color in HSV
#0-179 for hue, 0-255 for saturation and value
#240/360 = 120/180
lower_blue = np.array([110,255,255])
upper_blue = np.array([130,255,255])

mask = cv2.inRange(hsv,lower_blue,upper_blue)


gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

#edges = cv2.Canny(gray,50,150,apertureSize = 3)

# detect circles in the image
#circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 20,param1=50,param2=30,minRadius=0,maxRadius=0)
circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 20)
 
# ensure at least some circles were found
if circles is not None:
	# convert the (x, y) coordinates and radius of the circles to integers
	circles = np.round(circles[0, :]).astype("int")
 
	# loop over the (x, y) coordinates and radius of the circles
	for (x, y, r) in circles:
		# draw the circle in the output image, then draw a rectangle
		# corresponding to the center of the circle
		cv2.circle(output, (x, y), r, (0, 255, 0), 4)
		cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
 
	# show the output image
	cv2.namedWindow('output', cv2.WINDOW_NORMAL)
	cv2.imshow('output', np.hstack([mask]))
	cv2.waitKey(0)
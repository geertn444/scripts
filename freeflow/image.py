#Experiments with Image Recognition
#Reading in a screencapture from a freeflow game (starting position)
#And outputting the grid in coordinates

import numpy as np
import cv2

#print cv2.__version__

image = cv2.imread("example3.png")
output = image.copy()

h, w = image.shape[:2]

print "Image Size: " + str(w) + "x" + str(h)

#cv2.namedWindow('output', cv2.WINDOW_NORMAL)

#ISOLATING THE GRID

#Threshold the image: make all features white, the background black
#NOTE: future corner or line detection works much better on thresholded images
gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
ret,thresh1 = cv2.threshold(gray,50 ,255,cv2.THRESH_BINARY)


#cv2.imshow("output", np.hstack([thresh1]))
#cv2.waitKey(0)

#Then Contour Detection

im2, contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) 

#cv2.imshow("output", np.hstack([im2]))
#cv2.waitKey(0)

#Find the Largest Contour (this is the outer grid)

max_idx = 0
max_area = 0
for index,contour in enumerate(contours):
 area = cv2.contourArea(contour)
 if area > max_area:
  max_area = area
  max_idx = index

#print max_idx
#print max_area

#Masking out the grid from the rest of the image

mask = np.zeros_like(image) # Create mask where white is what we want, black otherwise
cv2.drawContours(mask, contours, max_idx, (255,255,255), -1) # Draw filled contour in mask

#cv2.imshow("output", np.hstack([mask]))
#cv2.waitKey(0)

out = np.zeros_like(image) # Extract out the object and place into output image
out[mask == 255] = image[mask == 255]

#cv2.imshow("output", np.hstack([out]))
#cv2.waitKey(0)


#OUT is the image we will be working with further

#DETECTING GRID SIZE

gray = cv2.cvtColor(out,cv2.COLOR_BGR2GRAY)
ret,thresh1 = cv2.threshold(gray,50 ,255,cv2.THRESH_BINARY)

# Then Edge Detection

sigma = 0.33
v = np.median(thresh1)
lower = int(max(0, (1.0 - sigma) * v))
upper = int(min(255, (1.0 + sigma) * v))

edges = cv2.Canny(thresh1,lower,upper,3,5)

#cv2.namedWindow('output', cv2.WINDOW_NORMAL)
#cv2.imshow("output", np.hstack([edges]))
#cv2.waitKey(0)



# Then Detect Lines (on the threshold)

lines = cv2.HoughLinesP(edges,1,np.pi/180,100,minLineLength = 500, maxLineGap = 20).tolist()

#Detect outer grid
xmin = w
ymin = h
xmax = 0
ymax = 0
#Filter lines close to each other (15 px)
#Outer loop
for line in lines:
 for x1,y1,x2,y2 in line:
  #print x1,y1,x2,y2
  #Inner loop
  #Detect outer points of the grid -> used later for cropping
  xmin = min(xmin,x1,x2)
  ymin = min(ymin,y1,y2)
  xmax = max(xmax,x1,x2)
  ymax = max(ymax,y1,y2)
  
  for index, line2 in enumerate(lines):
   for x3,y3,x4,y4 in line2:
        if y1==y2 and y3==y4: # Horizontal Lines
            diff = abs(y1-y3)
        elif x1==x2 and x3==x4: # Vertical Lines
            diff = abs(x1-x3)
        else:
            diff = 0

        if diff < 15 and diff is not 0:
            del lines[index]

#print lines
#print len(lines)
gridsize = str((len(lines)-2)/2)
print "Detected Gridsize: " + gridsize + "x" + gridsize
print "X(min): "+str(xmin)+" "+str(ymin)
print "Y(max): "+str(xmax)+" "+str(ymax)

#Draw lines on top of input

#for line in lines:
# for x1,y1,x2,y2 in line:
    
#    cv2.line(image,(x1,y1),(x2,y2),(0,255,0),2)

#cv2.namedWindow('output', cv2.WINDOW_NORMAL)
#cv2.imshow("output", np.hstack([image]))
#cv2.waitKey(0)




#Crop image for further processing
cropped = image[ymin:ymax, xmin:xmax]
cv2.namedWindow('cropped', cv2.WINDOW_NORMAL)
cv2.imshow("cropped", cropped)


h, w = cropped.shape[:2]


#Work further with the cropped image, so you don't have problems with the yellow colors
#Or text above or below the grid. This text gives erronoumous circles detection further on.


hsv = cv2.cvtColor(cropped, cv2.COLOR_BGR2HSV)

lower={}
upper={}

colors = ('b','y','o','r','g','f')

#FF DARK BLUE
lower['b'] = np.array([110,255,255])
upper['b'] = np.array([130,255,255])

#FF YELLOW
lower['y'] = np.array([25,255,230])
upper['y'] = np.array([35,255,240])

#FF ORANGE
lower['o'] = np.array([10,255,255])
upper['o'] = np.array([20,255,255])

#FF RED
lower['r'] = np.array([0,255,255])
upper['r'] = np.array([5,255,255])

#FF GREEN
lower['g'] = np.array([55,255,128])
upper['g'] = np.array([65,255,128])

#FF FUSIA
lower['f'] = np.array([85,255,255])
upper['f'] = np.array([95,255,255])


for color in colors:
 print "Detecting " + color
 mask = cv2.inRange(hsv,lower[color], upper[color])
 mask = cv2.GaussianBlur(mask, (9,9), 2)



 #cv2.namedWindow('output', cv2.WINDOW_NORMAL)
 #cv2.imshow("output", np.hstack([mask]))
 #cv2.waitKey(0)

#gray = cv2.cvtColor(result, cv2.COLOR_BGR2GRAY)


# detect circles in the image
 circles = cv2.HoughCircles(mask, cv2.HOUGH_GRADIENT, 1, 20,param1=50,param2=30,minRadius=0,maxRadius=0)
 
# ensure at least some circles were found
 if circles is not None:
	# convert the (x, y) coordinates and radius of the circles to integers
	circles = np.round(circles[0, :]).astype("int")
 
	# loop over the (x, y) coordinates and radius of the circles
	for (x, y, r) in circles:
		# draw the circle in the output image, then draw a rectangle
		# corresponding to the center of the circle
		#cv2.circle(output, (x, y), r, (0, 255, 0), 4)
		#cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
		print x,y
		print 1+x/(w/int(gridsize)),1+y/(h/int(gridsize))
	#cv2.namedWindow('output', cv2.WINDOW_NORMAL)
	# show the output image
	#cv2.imshow("output", np.hstack([output]))
	#cv2.waitKey(0)


#TODO: need to run a consistency check on the result
#TODO: remove colors not detected (or prevent errors)
#TODO: check if no colors return the same indexes
#TODO: Also note that sometimes three or more OVERLAPPING circles per color are returned (for example: ORANGE: 1x2 5x4 5x4 (last one needs to be ignored)



	
cv2.waitKey(0)
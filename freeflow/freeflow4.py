# Experiments solving Freeflow puzzles
# Experiments and exploration in Mathematical GRAPH theory

import string
import numpy as np
from itertools import product
from itertools import groupby
import itertools
import sys
import timeit
from multiprocessing import Pool

start = timeit.default_timer()

#global graph

#graph = {}

def colors_init(dim):
 color = {}
 letters = list(string.lowercase[:dim])
 for x in range(1,dim+1):
  for y in range(1,dim+1):
   indexstring = letters[x-1] + str(y)
   color[indexstring] = ''
 return color

def graph_init(dim):
 graph = {}
 #Init graph representing an YxY grid
 letters = list(string.lowercase[:dim])
 #print letters
 for x in range(1,dim+1):
  for y in range(1,dim+1):
   indexstring = letters[x-1] + str(y)
   #print indexstring
   graph[indexstring] = []
   #print graph[indexstring]
   if not(x==1):
    #not first row
	#d1.setdefault(key, []).append(value)
	graph.setdefault(indexstring,[]).append(letters[x-2]+str(y))
   if not(x==dim):
    #not last row
    graph.setdefault(indexstring,[]).append(letters[x-1+1]+str(y))
   if not(y==1):
    #not first column
	graph.setdefault(indexstring,[]).append(letters[x-1]+str(y-1))
   if not(y==dim):
    #not last column
	graph.setdefault(indexstring,[]).append(letters[x-1]+str(y+1))
    
 return graph

 
def find_path(graph, start, end, path=[]):
        path = path + [start]
        if start == end:
            return path
        if not graph.has_key(start):
            return None
        for node in graph[start]:
            if node not in path:
                newpath = find_path(graph, node, end, path)
                if newpath: return newpath
        return None
		
def find_all_paths(graph, start, end, path=[]):
        path = path + [start]
        if start == end:
            return [path]
        if not graph.has_key(start):
            return []
        paths = []
        for node in graph[start]:
            if node not in path:
                newpaths = find_all_paths(graph, node, end, path)
                for newpath in newpaths:
                    paths.append(newpath)
        return paths
		
def find_all_paths_around_colors(graph, start, end, exclude_nodes, path=[]):
        path = path + [start]
        if start == end:
            return [path]
        if not graph.has_key(start):
            return []
        paths = []
        for node in graph[start]:
            #print start + " -> " + node
			#check color also of candidate
			#BUG: we do not need to check the color of the end node, otherwise start == end and return path is never reached
            if (node not in path) and (node not in exclude_nodes or node == end):
                #print 'continue'
                newpaths = find_all_paths_around_colors(graph, node, end, exclude_nodes, path)
                for newpath in newpaths:
                    paths.append(newpath)
        return paths
		
def find_shortest_path(graph, start, end, path=[]):
        path = path + [start]
        if start == end:
            return path
        if not graph.has_key(start):
            return None
        shortest = None
        for node in graph[start]:
            if node not in path:
                newpath = find_shortest_path(graph, node, end, path)
                if newpath:
                    if not shortest or len(newpath) < len(shortest):
                        shortest = newpath
        return shortest

def convert_path_to_matrix(path):
#Converts a list of nodes ['a1','b1','c1'] to a matrix with the number 1 in the indexes
 matrix = np.zeros((gridsize, gridsize))
 for node in path:
  #print node
  #print ord(node[0])-96
  #print node[1]
  matrix[ord(node[0])-96-1,int(node[1])-1] = 1
  #print matrix
 return matrix
 
def overlap(indexes,matrixes):
 #indexes is a list of indexes, matrixes is a list of solution matrixes
 #get the appropriate matrixes, sum them and see if the matrix contains a "2" or higher number (only 0 or 1 is allowed)
 #this means an overlap has happened
 result = [matrixes[item[0]][item[1]] for item in enumerate(indexes)]
 #print sum(result)
 return (sum(result) > 1).any()
 
def remove_isolating_paths(path_list,neighb,start,stop):
 #loop through each path
 r = []
 for path in path_list:
  valid = True
  for n in neighb:
   #for each dot (except own start and stop dots), get neighbor cells and check if ALL of these cells are in path
   if (n != start) and (n != stop):
    #print neighb[n]
    if set(neighb[n]).issubset(path):
	 #print "Path isolates a dot"
	 valid = False
	 #print "Path: " + str(path)
	 #print "Neigh: " + str(neighb[n])
   
  if valid:
   r.append(path)
 #print r
 return r
 


 
if __name__ == '__main__':		

#color = {}

  

	if (len(sys.argv)<>2):
	 print "Program requires 1 command-line parameter: the filename of a puzzle."
	 sys.exit()
	try:
	 f = open(sys.argv[1], 'r')
	except IOError:
	 print "Could  not open file: ", sys.argv[1]
	 sys.exit()
	 
	dimx,dimy = f.readline().split()

	#NOTE:ONLY SQUARE MATRIXES SUPPORTED FOR NOW
	#Abort if not
	if (dimx<>dimy):
	 print "Non Square Puzzles not yet supported."
	 sys.exit()

	gridsize = int(dimx)
	print "Gridsize: " + dimx + "x" + dimx



	pos = {}
	idx = 0
	empty = []
	for line in f:
	 print line
	 line_idx =  chr(65+idx)
	 #print line_idx
	 idx += 1
	 for i in range(0,gridsize):
	  char = line[i]
	  if (char <> '.'):
	   v = line_idx + str(i+1)
	   #add position to a list, if not already exists, initialise list
	   pos.setdefault(char, []).append(v.lower())
	   #print pos

	graph = graph_init(gridsize)
	#print graph
	#Init empty color dictionary

	color = colors_init(gridsize)

	all_ones = np.ones((gridsize,gridsize),dtype=np.bool)

	#initialise grid with colors (startpositions)
	# test 5x5
	initial = pos.values()
	print "Initial Pairs: " + str(initial)
	#initial = [['a1','e2'],['a4','d2'],['a5','d3'],['d5','e3'],['b4','c3']]
	#initial = [['a4','d2'],['a2','d3'],['b4','c3']]

	#color = colors_exclude(color, initial)

	#Flatten the list of used nodes as a simple list
	exclude_nodes = list(itertools.chain(*initial))
	
	#Construct neighbor cells for each startcolorposition
	neighbors = {}
	for position in exclude_nodes:
	 neighbors[position] = []
	 #Add neighbor to the right if not at end of grid and not other color position
	 n_co = position[0] + str(int(position[1])+1)
	 if (int(position[1])+1 < gridsize) and n_co not in exclude_nodes:
	  neighbors[position].append(n_co)
	 #Add neighbor to the left if not at beginning of grid and not other color position
	 n_co = position[0] + str(int(position[1])-1)
	 if (int(position[1])-1 > 0) and n_co not in exclude_nodes:
	  neighbors[position].append(n_co)
	 #Add neighbor above if not at first row and not other color position
	 n_co = chr(ord(position[0])-1) + position[1]
	 #print n_co
	 if (ord(position[0])-1 > 96) and n_co not in exclude_nodes:
	  neighbors[position].append(n_co)
     #Add neighbor below if not at last row and not other color position
	 n_co = chr(ord(position[0])+1) + position[1]
	 #print n_co
	 if (ord(position[0])+1 < 96+gridsize+1) and n_co not in exclude_nodes:
	  neighbors[position].append(n_co)

	
	#Neighbors now contains, for each dot, the valid neighbors cells
	#This will be used to filter off "invalid" paths.
	#Indeed, a path for color blue that completely "encircles" another candidate (for example a red dot) is invalid because it isolates the red dot
	#By filtering off these paths, we lower our search matrix size
	print "Valid Neighbors: " + str(neighbors)
	print "Finding paths"

	solnumber = len(initial)
	sol = []
	#NOTE: don't use sol = [[]]*solnumber (the * makes references to the same list, if you append 1 it is reflected in all members
	
	#Search ALL possible paths for each color, taking into account other dots (so around other color dots)
	
	i = 0
	total_comb = 1
	for pair in initial:
	 sol.append(find_all_paths_around_colors(graph,pair[0],pair[1],exclude_nodes))
	 print "Paths for color " + str(i+1) + ": " + str(len(sol[i]))
	 
	 sol[i] = remove_isolating_paths(sol[i],neighbors,pair[0],pair[1])
	 
	 print "After filtering: " + str(len(sol[i]))
	 
	 total_comb = total_comb * len(sol[i])
	 #print sol[i]
	 #below is check for duplicate paths, should be none
	 #k = sorted(sol[i])
	 #dedup = list(k for k, _ in itertools.groupby(k))
	 #print len(dedup)
	 
	 i=i+1

	#OPTIMISATION: paths that run through any other initial pair should be eliminated.
	#We can run this optimisation afterwards, or we can modify the find_path procedure to exclude some nodes from the graph BEFORE running find_paths
	#Remember find_all_paths returns a list, so sol becomes a list of a list by itself
	#print sol
	#print
	#print sol[0] #all solutions for color 0
	#print
	#print sol[1] #first solution (path) for color 0
	#print
	#print sol[0][1]

	#sol_matrix = [[]]*solnumber
	#Create an empty list of lists
	sol_matrix = map(lambda x: [], range(solnumber))
	#print sol_matrix
	sol_number = []
	#Convert all found paths to matrixes
	i = 0
	print "Starting Loop"
	for sol_colors in sol:
	  #print sol_colors
	  sol_number.append(len(sol_colors))
	  for sol_colors_path in sol_colors:
	   #print sol_colors_path
	   sol_matrix[i].append(convert_path_to_matrix(sol_colors_path))
	  i=i+1
	 
	#total_comb = long(np.prod(np.array(sol_number)))
	print "Total solutions to check: " + str(total_comb)
	#Now loop through all possible solutions for all possible colors
	#print sol_matrix[0]
	#print sol_matrix[1]

	i = 0
	old_index = 0

	# process per core (4 cores assumed)
	#pool = Pool(processes=4)              
	# loop over all candidates in *sol_matrix (= all combinations), using 4 processors
	#pool.map(process_candidate, product(*sol_matrix))  
	
	#Create dictionary of index numbers
	idx = {}
	for i in range(solnumber):
	 idx[i] = range(len(sol_matrix[i]))
    
	print "Indexes created"
	progression = idx[0]
	#Convert list to list in list to get started (x is assumed to be a list in 5 lines)
	progression = [[x] for x in progression]
	#print d
	r = []
	for i in range(solnumber-1):
	 print "Combining " + str(len(progression)) + "x" + str(len(idx[i+1])) + "=" + str(len(progression)*len(idx[i+1]))
	 comb_matrix = [x + [y] for x,y in product(progression,idx[i+1])]
	 print "Done."
	 #comb_matrix = [x + [y] for x in progression for y in idx[i+1]]
	 #convert to list of list (NOTE: using list(comb_matrix) converts inside elements to tuples)
	 #comb_matrix = [list(i) for i in comb_matrix]
	 
	 
	 
	 #print comb_matrix
	 #print len(comb_matrix)
	 
	 cop = comb_matrix
	 for index,comb in enumerate(list(cop)):
	  #print index,comb
	  if overlap(comb,sol_matrix):
	   #print index,comb
	   #print "invalid"
	   #deleting deosn't work as index changes immediatly
	   #del r[index]
	   comb_matrix[index] = None
	  #else:
	   #print index,comb
	   #print "valid"	
	 #print comb_matrix # Contains still None
	 comb_matrix = [x for x in comb_matrix if x != None] # Filtering all "None" out
	 #print comb_matrix
	 print "Filtering overlaps. Result: "+str(len(comb_matrix))
	 progression = comb_matrix	 
	 #print sol_matrix[i]
	 #print sol_matrix[i+1]
	 #print comb
	
	#At the end of the previous loop, comb_matrix contains all matrix indexes of all color that when combined do not create overlap
	#NOTE: it does not mean that it is a valid solution, to check for a valid solution, there cannot be any 0 in the sum matrix (all positions need to be filled)
	#print sol_matrix[0]
	print "Checking Solutions."
	for sol in comb_matrix:
	 #print sol
	 #print list(enumerate(sol))
	 matrixes = [sol_matrix[x][y] for (x,y) in enumerate(sol)]
	 if np.array_equal(sum(matrixes),all_ones):
	  print
	  print "Solution found."
	  print
	  stop = timeit.default_timer()
	  print "{0:.2f} msec".format((stop - start)*1000) 
	  for m in matrixes:
	   print m
	
	 

	#product makes all possible matrix combinations possible
	#print items #can contain 1, 2, 3 items, depending on number of beginpairs/colors
	 
	#sum each individual paths
	#when paths overlap -> result will be 2 (or higher)
	#when paths leave an empty cell -> result will be 0 there
	#goal = result should be all ones matrix !!
	#print sum(items)
	#Only print 100 times % output, don't print if counter did not advance
	#Otherwise, print command takes most of the time :-)
	#new_index = int(100 * i / total_comb)
	#if (new_index<>old_index):
	# print "Checking solutions (" + str(new_index) + "%).\r",
	#old_index = new_index
	 
	#print i
	#i = i + 1

	print
	print "Done."


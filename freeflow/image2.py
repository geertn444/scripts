#Experiments with Image Recognition

import numpy as np
import cv2

print cv2.__version__

image = cv2.imread("screenshot.png")
output = image.copy()

#GRID SIZE DETECTION


#imgSplit = cv2.split(image)
#flag,b = cv2.threshold(imgSplit[2],0,255,cv2.THRESH_OTSU) 
#element = cv2.getStructuringElement(cv2.MORPH_CROSS,(1,1))
#cv2.erode(b,element)


#cv2.namedWindow('output', cv2.WINDOW_NORMAL)
#cv2.imshow("output", np.hstack([b]))
#cv2.waitKey(0)

#Filter on Grid Color

grid = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

lower_g = np.array([28,120,120])
upper_g = np.array([32,130,130])

mask = cv2.inRange(grid,lower_g, upper_g)

# Then Edge Detection

sigma = 0.33
v = np.median(mask)
lower = int(max(0, (1.0 - sigma) * v))
upper = int(min(255, (1.0 + sigma) * v))

edges = cv2.Canny(mask,lower,upper,3,5)

# Then Detect Lines

lines = cv2.HoughLinesP(edges,1,np.pi/180,100,minLineLength = 200, maxLineGap = 500)

total = len(lines)
print total

for i in range(0,total):
 for x1,y1,x2,y2 in lines[i]:
    
    cv2.line(image,(x1,y1),(x2,y2),(0,255,0),2)


cv2.namedWindow('output', cv2.WINDOW_NORMAL)
cv2.imshow("output", np.hstack([image]))
cv2.waitKey(0)











#image = cv2.medianBlur(image,5)

#gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

lower={}
upper={}

#yellow removed because of letters
colors = ('b','o','r','g','f')

#FF DARK BLUE
lower['b'] = np.array([110,255,255])
upper['b'] = np.array([130,255,255])

#FF YELLOW
#NOTE: the mage does contain yellow letters which gives false positives !! be aware !! need to fix this
lower['y'] = np.array([25,255,230])
upper['y'] = np.array([35,255,240])

#FF ORANGE
lower['o'] = np.array([10,255,255])
upper['o'] = np.array([20,255,255])

#FF RED
lower['r'] = np.array([0,255,255])
upper['r'] = np.array([5,255,255])

#FF GREEN
lower['g'] = np.array([55,255,128])
upper['g'] = np.array([65,255,128])

#FF FUSIA
lower['f'] = np.array([85,255,255])
upper['f'] = np.array([95,255,255])


for color in colors:
 print "Detecting " + color
 mask = cv2.inRange(hsv,lower[color], upper[color])
 mask = cv2.GaussianBlur(mask, (9,9), 2)



 #cv2.namedWindow('output', cv2.WINDOW_NORMAL)
 #cv2.imshow("output", np.hstack([mask]))
 #cv2.waitKey(0)

#gray = cv2.cvtColor(result, cv2.COLOR_BGR2GRAY)


# detect circles in the image
 circles = cv2.HoughCircles(mask, cv2.HOUGH_GRADIENT, 1, 20,param1=50,param2=30,minRadius=0,maxRadius=0)
 
# ensure at least some circles were found
 if circles is not None:
	# convert the (x, y) coordinates and radius of the circles to integers
	circles = np.round(circles[0, :]).astype("int")
 
	# loop over the (x, y) coordinates and radius of the circles
	for (x, y, r) in circles:
		# draw the circle in the output image, then draw a rectangle
		# corresponding to the center of the circle
		#cv2.circle(output, (x, y), r, (0, 255, 0), 4)
		#cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
		print x,y
	#cv2.namedWindow('output', cv2.WINDOW_NORMAL)
	# show the output image
	#cv2.imshow("output", np.hstack([output]))
	#cv2.waitKey(0)
import cherrypy

class Root:
    def mytest(self, param_1=None, param_2=None, *args, **kw):
        return repr(dict(param_1=param_1,
                         param_2=param_2,
                         args=args,
                         kw=kw))
    mytest.exposed = True

cherrypy.quickstart(Root())
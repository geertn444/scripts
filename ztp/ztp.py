#import cherrypy
from flask import Flask
from flask import request
from flask import jsonify
from flask import abort
from flask import make_response
from flask import request
from flask import render_template
from flask import flash
from flask import redirect
from flask import Response
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
import sys
import json
import requests
import re
import time
import pickle
import os

#from Cheetah.Template import Template

#website pages as compiled cheetah templates
#from html_index import html_index

#Switch object (to be staged)
class Switch:
 def __init__(self,serial,hostname,template):
  self.serial = str(serial)
  self.hostname = str(hostname)
  self.template = str(template)
  #self.fexes = {}
  #self.defaultvsan = str(vsan)
  #self.defaultvlan = str(vlan)




#read switchlist
try:
 with open('./list.pkl') as f:
   switchlist = pickle.load(f)
   f.close()
except IOError:
   print "Couldn't read previous data from file."
   switchlist = []


#x = Switch("AAA3","ddddd","access")
#switchlist.append(x)


class DeleteForm(Form):
    index = TextField('index')

class AddForm(Form):
    serial = TextField('serial')
    hostname = TextField('hostname')
    template = TextField('template')

class serial(Form):
    serial = TextField('serial')



app = Flask(__name__)

app.config.from_object(__name__)
app.config.update(dict(
    DEBUG = True,
    THREADED = True,
    SECRET_KEY = os.urandom(24)
))

title = 'GSK Vx Zero Touch Provisioning'


@app.route('/', methods=['GET', 'POST'])
def main_route():

    #data = render_template("index_jinja.html", title=title,switches=switchlist)
    #resp = Flask.Response(data)
    #resp.headers['Connection'] = 'close'
    return render_template("index_jinja.html", title=title,switches=switchlist)

@app.route('/delete', methods=['POST'])
def delete():
    form = DeleteForm(request.form)

    #index=request.form['index']
      
    print form.index.data

    switchlist.pop(int(form.index.data))


    #Save data to disk
    try:
     with open('./list.pkl','wb') as f:
      pickle.dump(switchlist,f)
      f.close()
      print "Data written to disk"
    except IOError:
     print "Couldn't write data to file."
    
    
    return redirect('/')
    #return render_template("index_jinja.html", title=title,switches=switchlist)


@app.route('/add', methods=['POST'])
def add():
    form = AddForm(request.form)
      
    print form.serial.data
    print form.hostname.data
    print form.template.data

    y = Switch(str(form.serial.data), str(form.hostname.data), str(form.template.data))
    switchlist.append(y)


    #Save data to disk
    try:
     with open('./list.pkl','wb') as f:
      pickle.dump(switchlist,f)
      f.close()
      print "Data written to disk"
    except IOError:
     print "Couldn't write data to file."

    
    return redirect('/')
    #return render_template("index_jinja.html", title=title,switches=switchlist)


@app.route('/gethostname', methods=['POST'])
def gethostname():
    form = serial(request.form)

    #index=request.form['index']
      
    reqserial = form.serial.data
    print reqserial

    for i in switchlist:
     print i.serial
     if i.serial == reqserial:
        print "match"
        result = i.hostname
        break
     else:
        print "nomatch"
        result = "Not found"
    
    return Response(result, mimetype='text/plain')
    #return render_template("index_jinja.html", title=title,switches=switchlist)

@app.route('/gettemplate', methods=['POST'])
def gettemplate():
    form = serial(request.form)

    #index=request.form['index']
      
    reqserial = form.serial.data
    print reqserial

    for i in switchlist:
     print i.serial
     if i.serial == reqserial:
        print "match"
        result = i.template
        break
     else:
        print "nomatch"
        result = "Not found"

    #resp = flask.Response(result)
    #resp.headers['Content-Type'] = 'text/plain'
    
    return Response(result, mimetype='text/plain')
    #return render_template("index_jinja.html", title=title,switches=switchlist)




@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    try:
     app.run(host='0.0.0.0',port=9000)
    except RuntimeError:
     print "error"





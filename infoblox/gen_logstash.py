#Python Script to write logstash config file
#Iterates through all *.txt files in a directory
#and creates a postprocessing configuration file for logstash, to enable src + dst ip site lookup
#NOTE: written config file is not yet optimal, since it processes src ips even when a match is already found
#NOTE: can be improved
#
# gyn94629
import os

print "#Programmatically generated config filter file"
print "filter {"
print " if [flow][src_addr] {"

for filename in os.listdir("."):
    if filename.endswith(".txt"): 
        # print(os.path.join(directory, filename))
        sitecode = filename.split(".")[0]
        print "  cidr {"
        print "   address => [ \"%{[flow][src_addr]}\" ]"
        print "   network_path => \"/local/etc/subnets/" + sitecode + ".txt\""
        print "   add_field => { \"[flow][src_site]\" => \"" + sitecode + "\" }"
        print "  }"

print " }"
print
print " if [flow][dst_addr] {"

for filename in os.listdir("."):
    if filename.endswith(".txt"): 
        # print(os.path.join(directory, filename))
        sitecode = filename.split(".")[0]
        print "  cidr {"
        print "   address => [ \"%{[flow][dst_addr]}\" ]"
        print "   network_path => \"/local/etc/subnets/" + sitecode + ".txt\""
        print "   add_field => { \"[flow][dst_site]\" => \"" + sitecode + "\" }"
        print "  }"

print " }"

print "}"
import xml.etree.ElementTree as ET
import os
tree = ET.parse('./out.xml')
root = tree.getroot()

#print root.tag
#print root.attrib

for site in root.iter('site'):
 code = site.get('code');
 print code;
 
 firstwrite = True
 for u in site.findall("./usage1[@type='SUMMARY']/usage2"):
  #need to loop again, because their can be more than one subnet in usage2
  for s in u.iter('subnet'):
   if s.text<>"":
    print s.text
    #prevent writing empty files with no info, so only open a file when you have something to write
    if firstwrite:
     f = open("./" + str(code) + ".txt","w");
     firstwrite = False
    f.write(s.text + '\n')
 if not firstwrite:
  #avoid writing an empty line at the end
  f.seek(-1, os.SEEK_END) # <---- 1 : len('\n')
  f.truncate()
  #normal f.close writes an empty line at the end
  #f.close()
# test = site.find('Usage1')

# print code,test
#public modules
import json
import requests
import sys

#own modules
import mypwd
import my_infoblox_lib as infoblox


username = "gyn94629"
password = mypwd.pwd

if len(sys.argv) < 2:
 #default
 print "You must specify an ip."

else:
 ip = sys.argv[1] 

 #initialize infoblox object
gridmaster = infoblox.blox('10.102.79.18',username,password)

#print gridmaster.get_subnet_info('159.114.115.0/24')
print "==========="
#result = gridmaster.get_ip_info_extattrs(ip)
result = gridmaster.get_next_available_ip(ip)
print result
print "==============="
#print gridmaster.get_ip_info_extattrs('159.114.115.124')['Reserved By']['value']
#print gridmaster.get_ip_info('159.114.115.1')
#print "==============="
#print gridmaster.get_ip_info('159.114.115.8')
##print "==============="
#print gridmaster.get_ip_info('159.114.115.21')
print "IP: " + str(ip)
print "Status: " + gridmaster.get_ip_info(ip)['status']
print "In DHCP Scope: " + str(gridmaster.in_dhcp_scope(ip))
print "In Reserved Range: " + str(gridmaster.in_reserved_range(ip))
if gridmaster.in_reserved_range(ip):
 print "Reserved By: " + gridmaster.get_ip_info_extattrs(ip)['Reserved By']['value']
print "Site: " + result['Site']['value']
print "Sitecode: " + result['SiteCode']['value']

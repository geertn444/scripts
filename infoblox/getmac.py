import json
import requests
import sys

import mypwd


my_headers = {'content-type': 'application/json-rpc'} 

username = "gyn94629"
password = mypwd.pwd


if len(sys.argv) < 2:
 #default
 print "You must specify a mac."
 sys.exit()
 #mac = '5c:b9:01:fc:9d:8c'

else:
 mac = sys.argv[1] 

 
print mac
 
url = "https://ibgm.bio.corpnet1.com/wapi/v1.2.1/record:host?mac:=" + mac
#url = "https://ibgm.bio.corpnet1.com/wapi/v1.2.1/ipv4address?ip_address=10.104.16.101&_return_fields=extattrs"
#url = "https://ibgm.bio.corpnet1.com/wapi/v1.2.1/record:a?ipv4addr=10.104.16.101&_return_fields=discovered_data"


#disables logging of invalid SSL cert - NOT ENCOURAGED !!
#import urllib3
#requests.disable_warnings()
requests.packages.urllib3.disable_warnings()

#payload = [{'jsonrpc': '2.0', 'method': 'cli', 'params': ['conf t',1], 'id': '1'}]
#my_data = json.dumps(payload)
#disables verification of SSL cert - NOT ENCOURAGED !!
response = requests.get(url, headers=my_headers,  auth=(username, password),verify=False)
#DEBUG: need to check for success !!!
print response.json()
#print
#print response.json()[0]['_ref']
#print response.json()[0]['name']
#print response.json()[0]['ipv4addrs']
#print response.json()[0]['view']
#print response.json()[0]['ipv4addrs'][0]['configure_for_dhcp']
#print response.json()[0]['ipv4addrs'][0]['_ref']
#print response.json()[0]['ipv4addrs'][0]['ipv4addr']
#print response.json()[0]['ipv4addrs'][0]['host']

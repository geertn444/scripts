<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="root">
			<root>
            <xsl:for-each-group select="site" group-by="@code">
             <site code="{@code}">
               <xsl:sequence select="current-group()/*"/>
             </site>
             </xsl:for-each-group>        
			</root>
    </xsl:template> 
</xsl:stylesheet>
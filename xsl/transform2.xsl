<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="node()|@*">
     <xsl:copy>
       <xsl:apply-templates select="node()|@*"/>
     </xsl:copy>
    </xsl:template>
 
    <xsl:template match="root/site">
	        <site>
			<!-- <xsl:sequence select="site"/> -->
            <xsl:for-each-group select="usage1" group-by="@type">
             <usage1 type="{@type}">
               <xsl:sequence select="current-group()/*"/>
             </usage1>
             </xsl:for-each-group>        
			 </site>
    </xsl:template> 
</xsl:stylesheet>
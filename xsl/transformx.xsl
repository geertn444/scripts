<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="root">
			<root>
            <xsl:for-each-group select="site" group-by="@code">
             <site code="{@code}">
               <!-- <xsl:sequence select="current-group()/*"/> -->
			   <xsl:apply-templates select="current-group()">
                <xsl:sort select="." data-type="text" order="ascending"/>
               </xsl:apply-templates>
             </site>
             </xsl:for-each-group>        
			</root>
    </xsl:template> 
	
	<xsl:template match="site">
	<xsl:for-each-group select="usage1" group-by="@type">
             <usage1 type="{@type}">
               <!-- <xsl:sequence select="current-group()/*"/> -->
			   <xsl:apply-templates select="current-group()">
                <xsl:sort select="." data-type="text" order="ascending"/>
               </xsl:apply-templates>
             </usage1>
    </xsl:for-each-group>        
	</xsl:template>
	
	<xsl:template match="site/usage1">
	<xsl:for-each-group select="usage2" group-by="@type">
             <usage2 type="{@type}">
               <xsl:sequence select="current-group()/*"/>
             </usage2>
    </xsl:for-each-group>        
	</xsl:template>
</xsl:stylesheet>
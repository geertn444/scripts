from netaddr import *

#NetAddr contains a procedure to do summarization in one single command
#Put individual subnets or IPAddresses in a plain list
#IPV4 and IPV6 can be mixed
ip_list = [ip for ip in IPNetwork('fe80::/120')]
ip_list.append(IPNetwork('192.0.2.0/24'))
ip_list.extend([str(ip) for ip in IPNetwork('192.0.3.0/24')])
ip_list.append(IPNetwork('192.0.4.0/25'))
ip_list.append(IPNetwork('192.0.4.128/25'))



#cidr_merge will summarize as much as possible
print cidr_merge(ip_list)
#[IPNetwork('192.0.2.0/23'), IPNetwork('192.0.4.0/24'), IPNetwork('fe80::/120')]

#If you want the summary commands, you have to compare the result list with the original list
#If in result list and not in original list -> then it is a summary command
import cherrypy
import sys
import json
import requests
import re
import time

from Cheetah.Template import Template

#website pages as compiled cheetah templates
from html_index import html_index

def naturalise(number):
#append zero's to numbers (only accept 0-999, else return 0)
 i = int(number)
 if i > 999:
  return "0"
 if i > 99:
  return str(number)
 if i > 9:
  return "0" + str(number)
 else:
  return "00" + str(number)
  
def VFCLookup(vfc):
#return fexid and slot from vfc number
 vfc = str(vfc)
 if (vfc[0]=='2'):
  return ('1' + vfc[1:3],str(int(vfc[3])+10))
 else:
  return (vfc[0:3],vfc[3])

#Switch object
class Switch:
 def __init__(self,url,username,password,vsan,vlan):
  self.url = str(url)
  self.username = str(username)
  self.password = str(password)
  self.fexes = {}
  self.defaultvsan = str(vsan)
  self.defaultvlan = str(vlan)
 
 def EnumerateFexes(self):
  payload = [{'jsonrpc': '2.0', 'method': 'cli', 'params': ['show fex detail',1], 'id': '1'}]
  my_data = json.dumps(payload)
  #disables verification of SSL cert - NOT ENCOURAGED !!
  response = requests.post(self.url, data=my_data, headers=my_headers,  auth=(self.username, self.password),verify=False)

  #print (response.text)

  result = response.json()['result']

  json_fex = result['body']['TABLE_fex_info']["ROW_fex_info"]

  #STUPID CISCO: returns a list when only one element, returns a DICT when multiple elements
  #NEED to catch this situation
  try:
   #Assume the data type is a list (multiple entries)
   #iterate through the list at ROW_area and print each area ID
   for f in json_fex:
    print f['chas_id']
    #MISSING LINES FOR MULTIPLE FEXES !!
  except TypeError:
   #The data type is NOT a list - need to handle like a single dictionary
   #Single entry
   print json_fex['chas_id']
   print json_fex['enclosure']
   print json_fex['rack']
   #Create FEX object
   f_obj = Fex(json_fex['chas_id'],json_fex['enclosure'],json_fex['rack'])
   #Create Dictionary of objects
   self.fexes[str(json_fex['chas_id'])] = f_obj
   
 def EnumerateFexInt(self):
 #On switch level, for all fexes at once
  payload = [{'jsonrpc': '2.0', 'method': 'cli', 'params': ['show int status',1], 'id': '1'}]
  my_data = json.dumps(payload)
  #disables verification of SSL cert - NOT ENCOURAGED !!
  response = requests.post(self.url, data=my_data, headers=my_headers,  auth=(self.username, self.password),verify=False)

  #print (response.text)

  result = response.json()['result']

  int_raw = result['body']['TABLE_interface']['ROW_interface']

  #STUPID CISCO: returns a list when only one element, returns a DICT when multiple elements
  #NEED to catch this situation
  #list = {}
  #idx = 0
  try:
   #Assume the data type is a list (multiple entries)
   #iterate through the list at ROW_area and print each area ID
   #NOTE: highly unlikely that 'show int status' only returns a single interface !!!
   #BUG FIX: 'name' = description = OPTIONAL, so it might not be defined/included for some interfaces -> need to catch this
   for int in int_raw:
    #Matches only FEX interfaces !!
    match = re.search(r'Ethernet(\d+)/(\d+)/(\d+)$',int['interface'])
    if match:
     slot = match.group(3)
     f = match.group(1)
     print int['interface']
     if 'name' not in int:
      print "No Description"
      desc = None
     else:
      desc = int['name']
     print int['state']
	 #Create Interface Object
     i = FexInt(int['interface'],desc,int['state'],int['vlan'])
     print "-----"
	 #Add to Dictionary
     #REMARK: index is naturalised.
     #Index of dict must be a string, but string sorts wrong: 1,2,100 is sorted 1,100,2
     #Index should be changed in 001,002,100 then sorts ok
     #Real slot will be stored as parameter inside object
     self.fexes[str(f)].intf[naturalise(slot)] = i
     #list[idx] = i
  except TypeError:
   #The data type is NOT a list - need to handle like a single dictionary
   #Single entry (very unlikely for interfaces, means only 1 interface was returned) ---> IMPOSSOBLE
   print "Error. Only 1 interface returned."
   
 def EnumerateVFCInt(self):
  #Need to write my own parser, since Cisco does not give back structured data for this command
  #NOTE: method = cli_ascii !!
  payload = [{'jsonrpc': '2.0', 'method': 'cli_ascii', 'params': ['show npv status',1], 'id': '1'}]
  my_data = json.dumps(payload)
  #disables verification of SSL cert - NOT ENCOURAGED !!
  response = requests.post(self.url, data=my_data, headers=my_headers,  auth=(self.username, self.password),verify=False)

  #print (response.text)
  result = response.json()['result']
  msg = result['msg']
  #Split string on "\n" into list of strings
  msg = [s.strip() for s in msg.splitlines()]
 
  # Interface: vfc1101, VSAN:  610, State: Up
  for line in msg:
   match = re.search(r'Interface: vfc(\d+),\s+VSAN:\s+(\d+), State: (\w+)$',line)
   if match:
    vfc = match.group(1)
    vsan = match.group(2)
    state = match.group(3)
    (fexid,slot) = VFCLookup(vfc)
    print fexid,slot,vfc,vsan,state
    self.fexes[fexid].intf[naturalise(slot)].vsan = vsan
    self.fexes[fexid].intf[naturalise(slot)].fcoe_state = state
   

#Fex object
class Fex:
 def __init__(self,fexid,enclosure,rack):
  self.fexid = fexid
  self.enclosure = enclosure
  self.rack = rack
  #Dictionary of FexInt objects
  self.intf = {}

#INTERFACE object
class FexInt:
 def __init__(self,intf,desc,state,vlan):
  self.intf = intf
  self.desc = desc
  self.state = state
  
  self.fexid = None
  self.slot = None
  self.vfc = None
  self.vsan = None
  self.fcoe_state = None
  
  self.vlan = str(vlan)
  #If Access, contains Access VLAN (as string)
  #If Trunk, contains "Trunk"
  
  
  #Example Ethernet101/0/15
  match = re.search(r'Ethernet(\d+)/(\d+)/(\d+)$',intf)
  if match:
   self.fexid = match.group(1)
   self.slot = match.group(3)
   
   #Calc VFC index number
   
   #101/0/1 -> VFC = 1011
   #101/0/5 -> VFC = 1015
   #101/0/10 -> VFC = 2010
   
   if (int(match.group(3)) < 10):
    self.vfc = str(self.fexid) + str(self.slot)
   else:
    self.vfc = str(int(self.fexid) + 100) + str(self.slot[-1])
   
 def EnableInterface(self,url,username,password):
  c = 'int ' + str(self.intf)
  payload = [{'jsonrpc': '2.0', 'method': 'cli', 'params': ['conf t',1], 'id': '1'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': [c,1], 'id': '2'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['no shut',1], 'id': '3'} \
   ]
  my_data = json.dumps(payload)
  #disables verification of SSL cert - NOT ENCOURAGED !!
  response = requests.post(url, data=my_data, headers=my_headers,  auth=(username, password),verify=False)
  #DEBUG: need to check for success


 
 #NOTE: for now, we pass the url and username parameters with the procedure
 #However, they can also be found in the parent classes, but i don't know how
 def DisableInterface(self,url,username,password):
  c = 'int ' + str(self.intf)
  payload = [{'jsonrpc': '2.0', 'method': 'cli', 'params': ['conf t',1], 'id': '1'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': [c,1], 'id': '2'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['shut',1], 'id': '3'} \
   ]
  my_data = json.dumps(payload)
  #disables verification of SSL cert - NOT ENCOURAGED !!
  response = requests.post(url, data=my_data, headers=my_headers,  auth=(username, password),verify=False)
  #DEBUG: need to check for success

  
 def EnableFCoEInterface(self,url,username,password,vsan):
  # Example:
  # interface vfc1104
  #  bind interface Ethernet110/1/4
  #  switchport trunk allowed vsan 610
  #  no shutdown
  # vsan database
  #  vsan 610 interface vfc1104
  # interface Ethernet110/1/4
  #  description FCOE Server Port
  #  switchport mode trunk 
  #  switchport trunk native vlan 999
  #  switchport trunk allowed vlan add 610
  #  spanning-tree port type edge trunk
  #  logging event port link-status
  #
  c = str(self.intf)
  vfc_idx = str(self.vfc)
  vsan_str = str(vsan)
  payload = [{'jsonrpc': '2.0', 'method': 'cli', 'params': ['conf t',1], 'id': '1'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['int ' + c,1], 'id': '2'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['description FCOE Server Port',1], 'id': '3'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['switchport mode trunk',1], 'id': '4'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['switchport trunk native vlan 999',1], 'id': '5'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['switchport trunk allowed vlan add ' + vsan_str,1], 'id': '6'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['spanning-tree port type edge trunk',1], 'id': '7'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['logging event port link-status',1], 'id': '8'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['interface vfc' + vfc_idx,1], 'id': '9'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['bind interface ' + c,1], 'id': '10'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['switchport trunk allowed vsan ' + vsan_str,1], 'id': '11'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['no shut',1], 'id': '12'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['vsan database',1], 'id': '13'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['vsan ' + vsan_str + ' interface vfc' + vfc_idx,1], 'id': '14'} \
   ]
  print payload
  my_data = json.dumps(payload)
  #disables verification of SSL cert - NOT ENCOURAGED !!
  response = requests.post(url, data=my_data, headers=my_headers,  auth=(username, password),verify=False)
  #DEBUG: need to check for success !!!

  
 def RemoveFCoEInterface(self,url,username,password,vsan):
  # Example:
  # no interface vfc1104
  #
  # NOTE: vsan database entry gets removed automatically after the above command
  # NOTE: this config leaves the interface in trunk mode (possibly with all vlans allowed)
  #
  # interface Ethernet110/1/4
  #  no description 
  #  switchport trunk allowed vlan remove 610
  c = str(self.intf)
  vfc_idx = str(self.vfc)
  #bug: need to determine default vsan number STILL !!
  vsan_str = str(vsan)
  payload = [{'jsonrpc': '2.0', 'method': 'cli', 'params': ['conf t',1], 'id': '1'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['int ' + c,1], 'id': '2'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['no description',1], 'id': '3'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['switchport trunk allowed vlan remove ' + vsan_str,1], 'id': '4'}, \
  {'jsonrpc': '2.0', 'method': 'cli', 'params': ['no interface vfc' + vfc_idx,1], 'id': '5'} \
   ]
  my_data = json.dumps(payload)
  #disables verification of SSL cert - NOT ENCOURAGED !!
  response = requests.post(url, data=my_data, headers=my_headers,  auth=(username, password),verify=False)
  #DEBUG: need to check for success !!!


users = {'user1': 'password1', 'user2': 'password2'}  


#WEBSITE object (CherryPy)
class Website(object):
	#default web page
    @cherrypy.expose
    def index(self,param_1=None,*args, **kw):
     global nexus_sipA
     global nexus_sipB
     print param_1
     print kw
     #print cherrypy.request.headers['Remote-User']
     
     if param_1=='enable':
      print "Enable" + str(kw['intf'])
      nexus_sipA.fexes[kw['fex']].intf[kw['intf']].EnableInterface(nexus_sipA.url,nexus_sipA.username,nexus_sipA.password)
      #Give some time to settle state
      time.sleep(1)
	  #Update/Read all interfaces
	  #NOTE: this is overkill, only one interface should be refreshed -> AJAX is the solution (to be implemented)
      nexus_sipA.EnumerateFexInt()
      nexus_sipA.EnumerateVFCInt()
     if param_1=='disable':
      print "Disable" + str(kw['intf'])
      nexus_sipA.fexes[kw['fex']].intf[kw['intf']].DisableInterface(nexus_sipA.url,nexus_sipA.username,nexus_sipA.password)
      #Give some time to settle state
      time.sleep(1)
	  #Update/Read all interfaces
	  #NOTE: this is overkill, only one interface should be refreshed -> AJAX is the solution (to be implemented)
      nexus_sipA.EnumerateFexInt()
      nexus_sipA.EnumerateVFCInt()
     if param_1=='fcoe_disable':
      print "Disable FCOE " + str(kw['intf'])
      nexus_sipA.fexes[kw['fex']].intf[kw['intf']].RemoveFCoEInterface(nexus_sipA.url,nexus_sipA.username,nexus_sipA.password,nexus_sipA.defaultvsan)
      nexus_sipB.fexes[kw['fex']].intf[kw['intf']].RemoveFCoEInterface(nexus_sipB.url,nexus_sipB.username,nexus_sipB.password,nexus_sipB.defaultvsan)
      #Give some time to settle state
      time.sleep(1)
	  #Update/Read all interfaces
	  #NOTE: this is overkill, only one interface should be refreshed -> AJAX is the solution (to be implemented)
      nexus_sipA.EnumerateFexInt()
      nexus_sipA.EnumerateVFCInt()
      nexus_sipB.EnumerateFexInt()
      nexus_sipB.EnumerateVFCInt()
     if param_1=='fcoe_enable':
      print "Enable FCOE " + str(kw['intf'])
      nexus_sipA.fexes[kw['fex']].intf[kw['intf']].EnableFCoEInterface(nexus_sipA.url,nexus_sipA.username,nexus_sipA.password,nexus_sipA.defaultvsan)
      nexus_sipB.fexes[kw['fex']].intf[kw['intf']].EnableFCoEInterface(nexus_sipB.url,nexus_sipB.username,nexus_sipB.password,nexus_sipB.defaultvsan)
      #Give some time to settle state
      time.sleep(1)
	  #Update/Read all interfaces
	  #NOTE: this is overkill, only one interface should be refreshed -> AJAX is the solution (to be implemented)
      nexus_sipA.EnumerateFexInt()
      nexus_sipA.EnumerateVFCInt()
      nexus_sipB.EnumerateFexInt()
      nexus_sipB.EnumerateVFCInt()
	
    
	 #generate index.html
     t = html_index()
     t.title = 'Fex Interface App'
     t.contents = 'Fex Interfaces'
     #give all fex objects to webpage
     t.fexes = nexus_sipA.fexes.values()
     t.fexesB = nexus_sipB.fexes.values()
     return t.respond()
	 
	 
	#test page for parameters passing
    @cherrypy.expose
    def test(self, param_1=None, param_2=None, *args, **kw):
     return repr(dict(param_1=param_1,param_2=param_2,args=args,kw=kw))
    #test.exposed = True


#GLOBALS
username = "gyn94629"
#password = "VGlzemF0bzE5".decode('Base64')
password = "Tiszato28"

my_headers = {'content-type': 'application/json-rpc'} 

urlA = "https://10.102.176.247:8000/ins"
urlB = "https://10.102.176.248:8000/ins"
 
if __name__ == '__main__':

 #disables logging of invalid SSL cert - NOT ENCOURAGED !!
 #import urllib3
 #requests.disable_warnings()
 requests.packages.urllib3.disable_warnings()
 
 nexus_sipA = Switch(urlA,username,password,610,300)
 nexus_sipA.EnumerateFexes()
 nexus_sipA.EnumerateFexInt()
 nexus_sipA.EnumerateVFCInt()

 nexus_sipB = Switch(urlB,username,password,660,300)
 nexus_sipB.EnumerateFexes()
 nexus_sipB.EnumerateFexInt()
 nexus_sipB.EnumerateVFCInt()

 
 print "====="
 
 cherrypy.quickstart(Website(),'/','fex_app.config')
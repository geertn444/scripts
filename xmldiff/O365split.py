#!/usr/bin/env python

import re
import sys

O365in = open(sys.argv[1], "r")
first = "start.xml"
f = open(first, 'w')

for line in O365in:
    match_object = re.search(r'<product name="(.*)">', line)
    if match_object:
     #new section started
     name = match_object.group(1)
     #BUGREPORT: should parse name to remove special characters or spaces
     #close existing file (not writing the current line)
     f.close()
     f = open("./"+name+".split.xml",'w')
     f.write(line)
    else:
     #no new section found, continue writing to output file
     f.write(line)
     
f.close()


import switch as sw
import logging

#logging.basicConfig(filename='myapp.log', level=logging.INFO)

#logger = logging.getLogger('Sample')
#ch = logging.StreamHandler()
#logger.addHandler(ch)

#basic config makes a screen logger by default
#logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# create a file handler
fh = logging.FileHandler('dallas.log')
fh.setLevel(logging.INFO)

# create console handler (screen)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

# add the fh to the logger
logger.addHandler(fh)

# add the ch to the logger
logger.addHandler(ch)

#GLOBALS
username = "gyn94629"
password = "VGlzemF0bzIx".decode('Base64')

#MS routes
msroutes = [
'10.4.24.5/32',
'10.4.24.6/32',
'10.65.0.152/30',
'10.96.251.0/29',
'40.108.39.192/27',
'70.37.183.0/27',
'130.23.123.0/25',
'130.23.123.128/26',
'130.23.123.192/29',
'130.23.123.240/28',
'130.23.126.32/27',
'130.23.126.160/27',
'130.23.126.192/27',
'130.23.126.224/28',
'134.177.96.224/27',
'134.177.101.112/28',
'134.177.211.128/26',
'141.251.104.0/26',
'141.251.104.64/26',
'141.251.104.128/26',
'141.251.104.192/26',
'141.251.128.0/26',
'141.251.128.64/26',
'141.251.128.128/26',
'141.251.128.192/26',
'141.251.131.0/26',
'141.251.131.64/26',
'141.251.131.128/26',
'141.251.131.192/26',
'141.251.175.128/26',
'141.251.175.192/26',
'141.251.185.0/26',
'141.251.185.64/26',
'141.251.206.0/26',
'141.251.206.64/26',
'141.251.206.128/26',
'141.251.206.192/26',
'141.251.207.0/26',
'141.251.207.64/26',
'141.251.207.128/26',
'141.251.207.192/26',
'192.32.55.0/26',
'192.56.74.64/27',
'192.56.74.96/29',
'192.56.74.192/26',
'199.160.96.197/32',
'209.123.131.192/28'
]

devices = {
'WAV':'10.96.8.1',
'RIX':'10.96.0.1',
'DRE':'10.96.36.3',
'SAE':'10.96.16.3',
'GOD':'10.96.40.3',
'GBX':'10.96.80.3',
'AND':'10.96.60.1',
'VIL':'10.96.64.1',
'MTT':'10.96.20.3',
'HTN':'10.96.24.3',
'LVL':'10.96.28.4',
'QBC':'10.96.32.4',
'GAS':'10.96.50.1',
'MET':'10.96.62.1',
'SGT':'10.96.44.4',
'SHE':'10.96.72.3'
 }

 


for dev in devices.keys():
 #print dev
 logger.info(dev)
 #print devices[dev]
 h = {'device_type': 'cisco_ios','ip':devices[dev],'username':username,'password':password,'secret':password}
 core = sw.Switch(h,logger)
 core.CheckRoutes(msroutes)



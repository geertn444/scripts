from netmiko import ConnectHandler
import sys
import time
import pandas as pd
import re
import ipaddress
import logging


#Custom Cisco Module
#Geert Nijs

#Switch object
class Switch:
 def __init__(self,handler,logger=None,verbose=False):
  self.username = str(handler['username'])
  self.password = str(handler['password'])
  self.ip = str(handler['ip'])
  self.verbose = verbose
  self.handler = handler
  self.ArpTable = {}
  self.logger = logger or logging.getLogger(__name__)
  
  
 def GetArp(self):
  net_connect = ConnectHandler(**self.handler)
  output = net_connect.send_command("show arp")
  net_connect.disconnect()

  #output is returned as a single string, so let's split this into lines in a list for iteration later
  output = output.split('\n')
  
  rows = []
  #parse output into a Pandas dataframe
  for lijn in output:
  #Internet  159.114.114.155         2   0050.5684.11bd  ARPA   Vlan364
  #Internet  159.114.125.148         0   0050.5686.4ba4  ARPA   Vlan315
  #Internet  193.74.130.12           0   Incomplete      ARPA
  #Internet  159.114.114.154         1   0050.56ad.3e8f  ARPA   Vlan364
  #Internet  159.114.124.148         1   0050.56a3.30fb  ARPA   Vlan313
  #Internet  193.74.132.10           0   0050.5686.720a  ARPA   Vlan310
  #
  
  # TODO: still need to filter out "Incomplete" records
  # TODO: still need to format ip address field (maybe into integer ??)
   print lijn
   matchObj = re.search(r'Internet\s+(.+)\s+[\d-]+\s+(.+)\s+ARPA\s+(.+)',lijn)
   if matchObj:
    print "matchObj.group(1) : ", matchObj.group(1)
    print "matchObj.group(2) : ", matchObj.group(2)
    print "matchObj.group(3) : ", matchObj.group(3)
    dict1 = {}
    dict1['ip'] =  matchObj.group(1)
    dict1['mac'] =  matchObj.group(2)
    dict1['vlan'] = matchObj.group(3)
    rows.append(dict1)
    
   else:
    print "No match!!"
  df = pd.DataFrame(rows)
  df['Time'] = pd.to_datetime('now')
  print df
  return df
  
 def GetBGPNeighbors(self):
  #Returns a list of BGP neighbors
  net_connect = ConnectHandler(**self.handler)
  output = net_connect.send_command("show ip bgp summ")
  net_connect.disconnect()

  #output is returned as a single string, so let's split this into lines in a list for iteration later
  output = output.split('\n')
  
  rows = []
  #parse output
  for lijn in output:
   #print lijn
   matchObj = re.search(r'^([\d\.]+)\s+\d\s+\d+\s',lijn)
   if matchObj:
    #print "matchObj.group(1) : ", matchObj.group(1)
    rows.append(matchObj.group(1))
   #else:
    #print "No match!!"
  return rows


 def GetBGPReceivedRoutes(self):
  #Returns a list of received BGP routes by neighbor
  lijst = self.GetBGPNeighbors()
  result = {}
  for neighbor in lijst:
   row = []
   cli = "show ip bgp neighbor " + str(neighbor) + " received-routes"
   net_connect = ConnectHandler(**self.handler)
   output = net_connect.send_command(cli)
   net_connect.disconnect()
   
   output = output.split('\n')
   
 #       Network          Next Hop            Metric LocPrf Weight Path
 #*>  10.4.20.64/29    193.74.134.5                  200      0 65273 13879 65408 i
 #*>  10.4.20.72/29    193.74.134.5                  200      0 65273 13879 65408 i
 #*>  10.116.232.192/26
 #                     193.74.134.5                  200      0 65273 13879 64800 65000 65523 65503 65501 i 
 # PRINCIPLE:
 # first need to match for a subnet X.X.X.X/Y
 # if this length > some characters -> then path is on next line
 # if this length < threshold -> then path is on the same line
 # NOTE: sometime subnet (/Y) is not there, so an optional match. /Y is then classfull standard subnet (except on 0.0.0.0 route)
 # NOTE: we do not generate the classfull subnet -> still to be done
 # NOTE: routes returned are (*) valid or (r) rib failures (see below)
   for i in range (0,len(output)):
    lijn = output[i]
    #print lijn
    matchObj = re.search(r'^\s(\*|r)\s+',lijn)
    if matchObj:
     #line with a route
     #print "Matching: " + lijn[5:23]
     subnet_re = re.search(r'^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(\/\d{1,2})?)',lijn[5:23])
     if subnet_re:
      subnet = subnet_re.group(1)
      #print "Subnet: " + subnet
      row.append(subnet)
      if len(subnet) < 17:
       path = lijn[63:-2]       
       #print "Path: " + path
      else:
       path = output[i+1][64:-2]
       #print "Path: " + path
   
   #print len(row)
   result[neighbor] = row
  return result
  
 def GetBGPAdvertisedRoutes(self):
  #Returns a dictionary of received BGP routes by neighbor
  lijst = self.GetBGPNeighbors()
  result = {}
  for neighbor in lijst:
   row = []
   cli = "show ip bgp neighbor " + str(neighbor) + " advertised-routes"
   net_connect = ConnectHandler(**self.handler)
   output = net_connect.send_command(cli)
   net_connect.disconnect()
   
   output = output.split('\n')
   
 #       Network          Next Hop            Metric LocPrf Weight Path
 #*>  10.4.20.64/29    193.74.134.5                  200      0 65273 13879 65408 i
 #*>  10.4.20.72/29    193.74.134.5                  200      0 65273 13879 65408 i
 #*>  10.116.232.192/26
   for i in range (0,len(output)):
    lijn = output[i]
    #print lijn
    matchObj = re.search(r'^\s(\*|r)\>\s+',lijn)
    if matchObj:
     #line with a route
     #print "Matching: " + lijn[5:23]
     subnet_re = re.search(r'^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(\/\d{1,2})?)',lijn[5:23])
     if subnet_re:
      subnet = subnet_re.group(1)
      #print "Subnet: " + subnet
      row.append(subnet)
     #there is no path variable in AdvertisedRoute
      
   #print len(row)
   result[neighbor] = row
  return result
 
 def CheckRoutes(self,routes,vrf=None):
  # "routes" should be a list of routes.
  # Returns a list: 1 if route can be found in routing table, 0 if not
  # This procedure is optimized for checking MULTIPLE routes (ie it does not disconnect/reconnect between the commands).
  #OPTIONALLY: supply a VRF to check the routes in
  net_connect = ConnectHandler(**self.handler)
  #enter enable mode
  result = []
  net_connect.enable()
  for route in routes:
   #BUG: need to check if parsing went ok -> will abort if parsing error
   net4 = ipaddress.ip_network(unicode(route))
   
   #Format subnet: "10.96.8.1 255.255.255.255"
   route_str = str(net4.network_address) + ' ' + str(net4.netmask)
  
   #TO BE ADDED: if detected format <ip> without subnet -> add classfull subnet mask
   if vrf is not None:
    cli = "show ip route vrf " + str(vrf)+ " " + str(route_str)
   else:
    cli = "show ip route " + str(route_str)
   #print "CLI: " + str(cli)
   self.logger.info('CLI: %s',cli)
   
   output = net_connect.send_command(str(cli))
   #output is returned as a single string
   #in this case, it is actually beneficial in order to simply check multiple lines
        
   #parse output
   #print "RESULT"
   #print output
   matchObj = re.search(r'Routing entry for',output)
   if matchObj:
    result.append(True)
    metric_search = re.search('Route metric is (.*),', output, re.IGNORECASE)
    tag_search = re.search('Route tag (.*)', output, re.IGNORECASE)
    #print cli + " --> OK, Metric:" + metric_search.group(1) + " Tag:" + tag_search.group(1)
    self.logger.info('%s ---> OK, Metric:%s Tag:%s',cli, metric_search.group(1),tag_search.group(1))
    
   else:
    result.append(False)
    #print cli + " --> NOT FOUND"
    self.logger.info("%S --> NOT FOUND",cli)
    #print "RESULT"
    self.logger.info('RESULT')
    #print output
    self.logger.info('%s',output)
    
  
  net_connect.disconnect()
  return result

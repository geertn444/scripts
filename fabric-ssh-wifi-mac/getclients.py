import paramiko
import time
import re


def disable_paging(remote_conn):
    '''Disable paging on a Cisco router'''

    remote_conn.send("terminal length 0\n")
    time.sleep(1)

    # Clear the buffer on the screen
    output = remote_conn.recv(1000)

    return output
	



if __name__ == '__main__':
 
 # VARIABLES THAT NEED CHANGED
 ip = '192.168.21.247'
 username = 'admin'
 password = 'admin'

    # Create instance of SSHClient object
 remote_conn_pre = paramiko.SSHClient()

    # Automatically add untrusted hosts (make sure okay for security policy in your environment)
 remote_conn_pre.set_missing_host_key_policy(
  paramiko.AutoAddPolicy())

    # initiate SSH connection
 remote_conn_pre.connect(ip, username=username, password=password, look_for_keys=False, allow_agent=False)
 print "SSH connection established to %s" % ip

    # Use invoke_shell to establish an 'interactive session'
 remote_conn = remote_conn_pre.invoke_shell()
 print "Interactive SSH session established"

    # Strip the initial router prompt
 output = remote_conn.recv(1000)

    # See what we have
 #print output

    # Turn off paging
 disable_paging(remote_conn)
 
 # Now let's try to send the router a command
 remote_conn.send("\n")
 remote_conn.send("show dot11 associations\n")

    # Wait for the command to complete
 time.sleep(2)
    
 output = remote_conn.recv(5000)

 
 lines = output.split('\r\n')
 
 #print lines
 
 #Known MAC addresses
 macs = { \
 '0024.d741.af30': 'Geert-Simac PC', \
 'c49a.024a.cf47': 'Geert-LG-G3', \
 'a44e.317a.0200': 'Geert-GSK PC', \
 '98b8.e3c2.6cc2': 'Nathalie-ipad', \
 '3038.55c3.47f5': 'Nathalie-Nokia', \
 'b876.3fe2.a5eb': 'Nathalie-SonyVAIO' \
 }
 
 #print output
 
 prog = re.compile("^(\w{4}\.\w{4}\.\w{4})")
 
 for line in lines:
  if prog.match(line):
   mac = prog.match(line).group(0)
   if mac in macs:
    print mac + "  " + macs[mac]
   else:
    print mac + "  unknown"
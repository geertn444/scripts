#! /usr/bin/python2.7
# -*- coding:utf-8 -*-

import os
from flask import Flask, jsonify
from flask import abort
from flask import make_response
from flask import request
import pickle

app = Flask(__name__)

app.config.from_object(__name__)
app.config.update(dict(
    DEBUG = False,
    THREADED = True,
    SECRET_KEY = os.urandom(24)
))

#positions = [
# {
# 'ticker': 'ERUS',
# 'pos' : 1,
# 'id' : 1
# },
# {
# 'ticker': 'XLF',
# 'pos' : 1,
# 'id' : 2
# },
# {
# 'ticker': 'IWM',
# 'pos' : 1,
# 'id' : 3
# },
# {
# 'ticker': 'MDY',
# 'pos' : 1,
# 'id' : 4
# },
# {
# 'ticker': 'XLE',
# 'pos' : 1,
# 'id' : 5
# },
# {
# 'ticker': 'XLI',
# 'pos' : 1,
# 'id' : 6
# },
# {
# 'ticker': 'XLB',
# 'pos' : 1,
# 'id' : 7
# },
# {
# 'ticker': 'EWT',
# 'pos' : 1,
# 'id' : 8
# },
# {
# 'ticker': 'DIA',
# 'pos' : 1,
# 'id' : 9
# },
# {
# 'ticker': 'EWC',
# 'pos' : 1,
# 'id' : 10
# },
#]
 
try:
 with open('./positions.bin', 'rb') as f:
  positions = pickle.load(f)
except:
 print "Could not read previous positions"
 positions = []
 
print "=== PREVIOUS ======"
print positions
print "==================="

@app.route('/')
def page_index():
    return "<h1>Hello</h1>"
    
@app.route('/api/v1.0/save', methods=['GET'])
def save_pos():
 try:
  with open('./positions.bin', 'wb') as f:
   pickle.dump(positions, f)
 except:
  abort(404) 
 return ""
    
@app.route('/api/v1.0/pos', methods=['GET'])
def get_tasks():
    return jsonify({'positions': positions})

@app.route('/api/v1.0/pos/<int:pos_id>', methods=['GET'])
def get_task(pos_id):
    pos = [pos for pos in positions if pos['id'] == pos_id]
    if len(pos) == 0:
        abort(404)
    return jsonify({'pos': pos[0]})
    
@app.route('/api/v1.0/add', methods=['POST'])
def create_pos():
    if not request.json or not 'ticker' in request.json:
        abort(400)
#BUG REPORT: need to check if ticker already exists, if so -> add to existing, if not -> create new pos
#in fact, add does not need to specify any ids, we need to look for them ourselves
    send_tick = request.json['ticker'].upper()          #need to check that ticker is unicode string
    pos = [pos for pos in positions if pos['ticker'] == send_tick]
    if len(pos) == 0:
    #not yet in positions. Action: add
     pos = {
        'id': positions[-1]['id'] + 1,
        'ticker': request.json['ticker'],
        'pos': 1
     }
     positions.append(pos)
     return jsonify({'pos': pos}), 201
    else:
     #already a position. Action: add number of shares to existing position
     try:
      send_number = int(request.json['pos'])
     except:
      send_number = 1
     
     pos[0]['pos'] = pos[0]['pos'] + send_number
     return jsonify({'pos': pos[0]})


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=9000)
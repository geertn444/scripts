import numpy as np
import pandas as pd
import datetime as dt
from pandas.tseries.offsets import *
#import pandas_datareader as web
import pandas.io.data as web
import matplotlib.pyplot as plt
import sys
import pickle
import argparse


#central place to determine list
import etf_list

ticker_list = etf_list.tickers

parser = argparse.ArgumentParser(description='ETF Momentum Trading Simulation.')
parser.add_argument('-d', dest='date', default = (dt.datetime.today() - dt.timedelta(days=1)), help='Date in the past to \
start simulation. Default = yesterday')
parser.add_argument('-s', action='store_true', default = False, help='Save result of simulation as the new portfolio. Default = no')

result = parser.parse_args()

d_end = pd.to_datetime(result.date)
d_start = d_end - dt.timedelta(days=370)

print d_start
print d_end

#first business day of this month
#month_begin = vandaag - BMonthBegin()
#start12 = month_begin - BMonthBegin(12)
#start6 = month_begin - BMonthBegin(6)
#start3 = month_begin - BMonthBegin(3)
#start1 = month_begin - BMonthBegin(1)
#end = month_begin
#start = start12
#beginning of previous month
#start = pmonth - MonthBegin()
#end of previous month
#end = pmonth + MonthEnd()
#print start12,start6,start3,start1
#print end

#NOTE: i am not going to use this, because even with BMonthBegin it can still be 2016-01-01 which is a holiday, so not present in index
#Better is to just cut the list in 4 parts

try:
 with open('./positions.bin', 'rb') as f:
  positions = pickle.load(f)
except:
 print "Could not read previous positions"
 positions = []
 
print "=== PREVIOUS ======"
print positions
print "==================="

price_change_12m = {}
price_change_6m = {}
price_change_3m = {}
price_change_1m = {}
ave = {}
m_ave = {}

for ticker in ticker_list:
    try:
     prices = web.DataReader(ticker, 'yahoo', d_start, d_end)
    except:
     print "Error getting " + ticker + " . Skipping stock."
     print sys.exc_info()[0]
    else:
     closing_prices_12m = prices['Close']
     #print closing_prices_12m
     if len(closing_prices_12m) < 252:
      print "Lookback period not long enough. Exiting."
      exit()
     change_12m = (closing_prices_12m[-1]-closing_prices_12m[-252])/closing_prices_12m[-252]
     change_1m = (closing_prices_12m[-1]-closing_prices_12m[-20])/closing_prices_12m[-20]
     change_3m = (closing_prices_12m[-1]-closing_prices_12m[-60])/closing_prices_12m[-60]
     change_6m = (closing_prices_12m[-1]-closing_prices_12m[-125])/closing_prices_12m[-125]
     #m1 = closing_prices_12m[-1]/closing_prices_12m[-252]
     #m2 = closing_prices_12m[-1]/closing_prices_12m[-20]
     #m3 = closing_prices_12m[-1]/closing_prices_12m[-60]
     #m4 = closing_prices_12m[-1]/closing_prices_12m[-125]
     average = (change_6m + change_1m + change_3m + change_12m)/4
     #m_a = (m1+m2+m3+m4)/4
     name = ticker_list[ticker]
     price_change_12m[ticker] = change_12m
     price_change_1m[ticker] = change_1m
     price_change_3m[ticker] = change_3m
     price_change_6m[ticker] = change_6m
     ave[ticker] = average
     #m_ave[ticker] = m_a
    
     

pc12 = pd.Series(price_change_12m)
pc6 = pd.Series(price_change_6m)
pc3 = pd.Series(price_change_3m)
pc1 = pd.Series(price_change_1m)
x   = pd.Series(ave)
#a   = pd.Series(m_ave)
df = pc12.to_frame(name='12month')
df['6month'] = pd.Series(pc6, index=df.index)
df['3month'] = pd.Series(pc3, index=df.index)
df['1month'] = pd.Series(pc1, index=df.index)
df['average'] = pd.Series(x,index=df.index)
#df['ave_m'] = pd.Series(a,index=df.index)
print "=== DF ==="
print df
print "======"

#Remove negative
#We never buy a stock with neg avg momentum, even if he makes top 10, but we don't filter them out (this would change rebalancing weights)
#x = x[x > 0]

x.sort_values(inplace=True,ascending=False)
r = x.nlargest(10)
#fig, ax = plt.subplots(figsize=(10,8))
#r.plot(kind='bar', ax=ax)
print "===== TOP10 ========="
print r
print "====================="
print "Old portfolio"
print positions
print "====================="

for stock in positions:
 if stock not in r.index.values:
  print "Need to sell : " + stock + ". Reason: not in TOP 10 anymore."
 else:
  if price_change_1m[stock] < 0:
   print "Need to sell : " + stock + ". Reason: negative momentum this month."

port_n = []
  
#BUG REPORT: it can be that new value 1 month momentum is negative and still positive in TOP10 . What to do then ?  
for stock in r.index.values:
 if (price_change_1m[stock] > 0) and (ave[stock] > 0):
 #Only buy into positive momentum
  if stock not in positions:
   print "Need to buy: " + stock + ". Reason: newcomer in TOP 10 w pos momentum (short & long term)."
   port_n.append(stock)
  else:
   print "Need to rebalance existing position in " + stock + " to equal weight."
   port_n.append(stock)
 else:
  if price_change_1m[stock] < 0:
   print "Didn't buy " + stock + " because of negative short term momentum."
  if ave[stock] < 0:
   print "Didn't buy " + stock + " because of negative long term momentum."

  
print port_n

if result.s:
 with open('./positions.bin', 'wb') as f:
  pickle.dump(list(port_n), f)
  print "Saved to portfolio to disk."
else:
 print "Not Saving new data."
#except:
# print "Could not write new positions."

#df = pc12.to_frame(name='12month')
#print df

#===== TOP10 =========
#EWZ     0.160450
#EPU     0.140874
#ENZL    0.131448
#XLU     0.114296
#TUR     0.114230
#ILF     0.092766
#XLP     0.082309
#EIDO    0.075488
#XLK     0.074955
#ERUS    0.068699
#dtype: float64

#===== TOP10 =========
#EPU     1.243576
#EWZ     1.127494
#SLV     1.111186
#ENZL    1.102662
#TUR     1.094168
#XLB     1.074022
#ERUS    1.072762
#ILF     1.063134
#IAU     1.055587
#XLI     1.053860
#dtype: float64
#=====================

import numpy as np
import pandas as pd
import datetime as dt
from pandas.tseries.offsets import *
# import pandas_datareader as web
import pandas.io.data as web
import matplotlib.pyplot as plt
import sys
import pickle
import argparse

parser = argparse.ArgumentParser(description='Calculate Covariance between 2 stocks')
parser.add_argument('-d', dest='date', default=(dt.datetime.today() - dt.timedelta(days=1)), help='Date in the past to start calculation. Default = yesterday')
parser.add_argument('tickers', metavar='T', nargs='+',
                    help='Two Tickers (not more)')

result = parser.parse_args()

d_end = pd.to_datetime(result.date)
d_start = d_end - dt.timedelta(days=370)

print d_start
print d_end



#closes = []

df = pd.DataFrame()
i=0
d = []

for ticker in result.tickers:
    i=i+1
    try:
        prices = web.DataReader(ticker, 'yahoo', d_start, d_end)
    except:
        print "Error getting " + ticker + " . Skipping stock."
        print sys.exc_info()[0]
    else:

        df['close' + str(i) ] = prices['Adj Close']


df['close1s'] = df['close1'].shift(1)
df['r1'] = 100*((df['close1'] - df['close1s']) / df['close1s'])

df['close2s'] = df['close2'].shift(1)
df['r2'] = 100*((df['close2'] - df['close2s']) / df['close2s'])

#drop first row (since this one is NA because of shift, no return known for first row
df.drop(df.index[[0]],inplace=True)

d.append(list(df['r1'].values.T))
d.append(list(df['r2'].values.T))

print df[['close1','close2','r1','r2']]

covmat=np.cov(d)

print(covmat)




import pandas as pd
import pandas.io.sql as pd_sql
import numpy as np
from sqlalchemy import create_engine
import sqlite3 as lite
from datetime import datetime,timedelta
import sys

class stock:

 def __init__(self,ticker):
  self.ticker = ticker
  con = lite.connect(".//stocks.db")
  if len(sys.argv) > 1:
   sql = "SELECT * FROM daily where ticker = '" + str(ticker) + "' and datum <= '" + str(sys.argv[1]) + "' ORDER BY datum DESC"
  else:
    sql = str("SELECT * FROM daily where ticker = '" + ticker + "' ORDER BY datum DESC LIMIT 300")
  self.data = pd.read_sql(sql,con, index_col="datum")
  if not(self.data.empty):
   self.open = self.data["open"][0]
   self.volume = self.data["volume"][0]
   self.days = len(self.data)
  else:
   self.open = 0
   self.volume = 0
  self.mean2 = pd.stats.moments.rolling_mean(self.data["close"].sort_index(ascending=True),2).sort_index(ascending=False)
  self.mean5 = pd.stats.moments.rolling_mean(self.data["close"].sort_index(ascending=True),5).sort_index(ascending=False)
  self.mean50 = pd.stats.moments.rolling_mean(self.data["close"].sort_index(ascending=True),50).sort_index(ascending=False)
  self.mean200 = pd.stats.moments.rolling_mean(self.data["close"].sort_index(ascending=True),200).sort_index(ascending=False)
  #self.52weekhigh
  #self.52weeklow
  con.close()
  #print self.open
  #print self.close
  #print self.volume

  
 def data(self):
  print self.data
  
 def close(self,datum=None):
  if (self.data.empty): return 0
  if datum==None:
   return self.data["close"][0]
  else:
   return self.data["close"][datum]
 
 
 def quote(self):
  return self.ticker
  
 def datumx(self):
  #need to check if data is not empty
  if (self.data.empty): return "0"
  return self.data.index[0]
 
  
 def sma2(self,datum=None):
  if (self.mean2.empty): return 0
  if datum==None:
   return self.mean2[0]
  else:
   return self.mean2[datum] 
  
 def sma5(self,datum=None):
  if (self.mean5.empty): return 0
  if datum==None:
   return self.mean5[0]
  else:
   return self.mean5[datum]
 
 def sma200(self,datum=None):
  if (self.mean200.empty): return 0
  if datum==None:
   return self.mean200[0]
  else:
   return self.mean200[datum]
   
 def sma50(self,datum=None):
  if (self.mean50.empty): return 0
  if datum==None:
   return self.mean50[0]
  else:
   return self.mean50[datum]
 
 def dsma5(self,datum=None):
  if (self.mean5.empty): return 0
  if datum==None:
   return self.mean5[0]-self.mean5[1]
 
  
 def dsma200(self,datum=None):
  if (self.mean200.empty): return 0
  if datum==None:
   return self.mean200[0]-self.mean200[1]
 
 
  

#engine = create_engine('mysql://python:python@192.168.21.246/energy')

maindb = lite.connect(".//stocks.db")
stocklist = pd.read_sql("SELECT ticker,region FROM stocks",maindb)
maindb.close()

#Print Parameters
#for tick in stocklist.values:
 #print tick[0]
 #inv = stock(tick[0])
 #print inv.datumx()
 #print "Close: " + str(inv.close()) + " Volume: " + str(inv.volume)
 #print "SMA5: "+str(inv.sma5()) + " SMA50: "+str(inv.sma50())+" SMA200: "+str(inv.sma200())

 

# Close > 200 day average
print
print "MODEL 1 : close > sma200"
print
for tick in stocklist.values:
 #print tick[0]
 inv = stock(tick[0])

 #print inv.data

 #print inv.quote()
 #print inv.close
 #print inv.datumx()
 #print inv.sma200()
 #print pd.isnull(inv.sma200())
 if (inv.close() > inv.sma200()):
  print inv.datumx() + " " + inv.ticker
  #print inv.sma200(0)
  #print inv.sma200(1)
  #print inv.dsma200()

 #print inv.sma5()
 #print inv.sma200()

 #print inv.dsma5()
 #print inv.dsma200()
 
# Close > 200 day average for last 20 days
print
print "MODEL 2 : close > sma200 for last 20 days"
print
for tick in stocklist.values:
 #print tick[0]
 inv = stock(tick[0])
 result = 1
 if (inv.days<19):
  print "Skipping stock "+inv.ticker+". No 20 days of data."
 else:
  for i in range(0,19):
   #print inv.close(i) + inv.sma200(i)
   if (inv.close(i) < inv.sma200(i)) or (pd.isnull(inv.sma200(i))): result = 0
  if result == 1: print inv.datumx() + " " + inv.ticker
 
# golden crossing upwards
print
print "MODEL 3 : MA50/200 Golden Cross UP (1 day)" 
print
for tick in stocklist.values:
 #print tick[0]
 inv = stock(tick[0])
 if (inv.sma50() > inv.sma200()) and (inv.sma50(1) < inv.sma200(1)): print inv.datumx() + " " + inv.ticker
 

# golden crossing upwards
print
print "MODEL 4 : Bullish MA50/200 Golden Cross UP (20 days)"
print "Very strong reversal trend UP" 
print
for tick in stocklist.values:
 #print tick[0]
 inv = stock(tick[0])
 result = 1
 #print str(inv.sma50()) + " " + str(inv.sma50(0))
 if (inv.days<20):
  #do nothing , don't run for loop as index i will get out of range
  #print "Skipping stock "+inv.ticker+". No 20 days of data"
  pass
 else:
  for i in range(1,20):
   #sma50 must be below sma200 for last 20 days, else put result = 0
   if (inv.sma50(i) > inv.sma200(i)): result = 0
  
  if (inv.sma50() > inv.sma200()) and (result == 1) : print inv.datumx() + " " + inv.ticker
 
# golden crossing downward
print
print "MODEL 5 : MA50/200 Golden Cross DOWN (1 day)" 
print
for tick in stocklist.values:
 inv = stock(tick[0])
 #print tick[0] + " volume: " + str(inv.volume) + " close: "+str(inv.close())
 #print tick[0] + " sma2: "+str(inv.sma2())
 #print tick[0]  + " " + str(inv.sma50()) +" " + str(inv.sma200())
 #print tick[0]  + " " + str(inv.sma50(1)) + " "+ str(inv.sma200(1))
 if (inv.sma50() < inv.sma200()) and (inv.sma50(1) > inv.sma200(1)): print inv.datumx() + " " + inv.ticker
 

# golden crossing downward
print
print "MODEL 6 : Bearish MA50/200 Golden Cross (20 days)"
print "Very strong reversal trend DOWN, sell trigger" 
print
for tick in stocklist.values:
 #print tick[0]
 inv = stock(tick[0])
 result = 1
 if (inv.days<20):
  #do nothing, don't run for loop since index will get out of range
  pass
 else:
  for i in range(1,20):
   #sma50 must be above sma200 for last 20 days, else put result = 0
   if (inv.sma50(i) < inv.sma200(i)): result = 0
  
  if (inv.sma50() < inv.sma200()) and (result == 1) : print inv.datumx() + " " + inv.ticker
 

print
print "MODEL 7 : Rising sma200 for 20 days in a row"
print "Strong trend up indicator" 
print
for tick in stocklist.values:
 #print tick[0]
 inv = stock(tick[0])
 result = 1
 if (inv.days >= 19):
  for i in range(0,19):
   #print str(inv.sma200(i)) + " " + str(inv.sma200(i+1))
   if (inv.sma200(i) < inv.sma200(i+1)) or (pd.isnull(inv.sma200(i))): result = 0
  #BUG FIX: if one sma is equal to 0, it means not enough sma data is available -> force no result
  if (result == 1) : print inv.datumx() + " " + inv.ticker
  else:
   pass
   #print "Skipping stock "+inv.ticker+". No data for 10 days"

print
print "MODEL 8 : Falling sma200 for 20 days in a row"
print "Long term trend down indicator" 
print
for tick in stocklist.values:
 #print tick[0]
 inv = stock(tick[0])
 result = 1
 if (inv.days >=19):
  for i in range(0,19):
   #print inv.sma200(i)
   if (inv.sma200(i) > inv.sma200(i+1)) or (pd.isnull(inv.sma200(i))): result = 0
  #BUG FIX: if one sma is equal to 0, it means not enough sma data is available -> force no result
  if (result == 1) : print inv.datumx() + " " + inv.ticker
 else:
  pass

print
print "MODEL 9 : Falling through rising sma200, look only at US stocks !!"
print "Trade idea: most of the time triggers large sell offs in the US, wait until bottom, often a (small) recovery of around 10% is seen afterwards" 
print
for tick in stocklist.values:
 region = tick[1]
 inv = stock(tick[0])
 #fall through sma200
 cond1 = (inv.close() < inv.sma200()) and (inv.close(1) > inv.sma200(1))
 #rising sma200
 cond2 = (inv.sma200() > inv.sma200(1))
 #only US
 cond3 = (region == 'US')
 print inv.ticker + ", Fall below sma200: " + str(cond1) + ", Rising sma200: " + str(cond2)+ ", US Market: " + str(cond3)
 
 if (cond1 and cond2 and cond3):
  print inv.datumx() + " " + inv.ticker
 
 #result = 1
 #if (inv.days >=19):
 # for i in range(0,19):
 #  if (inv.sma200(i) > inv.sma200(i+1)): result = 0
 # 
 # if (result == 1) : print inv.datumx() + " " + inv.ticker
 #else:
 # pass  
 

#data = pd.read_sql("SELECT * FROM daily where ticker = 'KBC.BR' ORDER BY datum DESC LIMIT 100",con)
#print data
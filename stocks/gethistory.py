from pandas.io.data import DataReader
from datetime import datetime
import sqlalchemy


#start = datetime.datetime(2010, 1, 1)

#end = datetime.datetime(2013, 1, 27)


#!/usr/bin/python

import sys

if len(sys.argv)<>2:
 print "Please specify 1 arg : ticker symbol"
 sys.exit()

#print sys.argv[1]

stockhistory = DataReader(sys.argv[1],"yahoo")
#Drop "Adj Close" column

if 'Adj Close' in stockhistory.columns:
      #drop 'sourceport' but first check for existance
      stockhistory = stockhistory.drop('Adj Close',1)
	  
#change column names to lowercase
stockhistory = stockhistory.rename(columns={'Low': 'low', 'High': 'high','Close':'close','Open':'open','Volume':'volume'})

#change index name from date to datum
stockhistory.index.names = ['datum']	  
#Add ticker column
stockhistory['ticker'] = sys.argv[1] 
#Add difference column (is easier here then in sql)
stockhistory['change'] = stockhistory['close'] - stockhistory['close'].shift(+1)
#round results of change, otherwise problems during upload (change = 0.119999999999999 for example)
stockhistory['change'] = map(lambda x: round(x,2),stockhistory['change'])
stockhistory['low'] = map(lambda x: round(x,2),stockhistory['low'])
stockhistory['high'] = map(lambda x: round(x,2),stockhistory['high'])
stockhistory['close'] = map(lambda x: round(x,2),stockhistory['close'])
stockhistory['open'] = map(lambda x: round(x,2),stockhistory['open'])


print stockhistory
print stockhistory.dtypes
print "Upload to database (Y/N) ? ",
choice = raw_input()
if choice=="Y":
 print "Uploading"
 engine = sqlalchemy.create_engine('sqlite:///stocks.db')
 print stockhistory
 #warning: do not select 'replace' , it erases complete table !
 stockhistory.to_sql('daily', engine, if_exists='append', index=True)
else:
 print "Too bad"

import pandas as pd
import pandas.io.sql as pd_sql
import numpy as np
from sqlalchemy import create_engine
import sqlite3 as lite
from datetime import datetime,timedelta

class stock:

 def __init__(self,ticker):
  self.ticker = ticker
  con = lite.connect(".//stocks.db")
  self.data = pd.read_sql("SELECT * FROM daily where ticker = '" + ticker + "' ORDER BY datum DESC LIMIT 300",con, index_col="datum")
  self.mean5 = pd.stats.moments.rolling_mean(self.data["close"].sort_index(ascending=True),5).sort_index(ascending=False)
  self.mean200 = pd.stats.moments.rolling_mean(self.data["close"].sort_index(ascending=True),200).sort_index(ascending=False)
  con.close()

  
 def data(self):
  print self.data
 
 def quote(self):
  return self.ticker
  
 def datumx(self):
  return self.data.index[0]
  
 def sma5(self,datum=None):
  if datum==None:
   return self.mean5[0]
  else:
   return self.mean5[datum]
 
 def sma200(self,datum=None):
  if datum==None:
   return self.mean200[0]
  else:
   return self.mean200[datum]
 
 def dsma5(self,datum=None):
  if datum==None:
   return self.mean5[0]-self.mean5[1]
 
  
 def dsma200(self,datum=None):
  if datum==None:
   return self.mean200[0]-self.mean200[1]
 
 
  

#engine = create_engine('mysql://python:python@192.168.21.246/energy')

maindb = lite.connect(".//stocks.db")
stocklist = pd.read_sql("SELECT ticker FROM stocks",maindb)
print stocklist
maindb.close()

inv = stock("KBC.BR")

print inv.data

print inv.quote()

print inv.datumx()

print inv.sma5()
print inv.sma200()

print inv.dsma5()
print inv.dsma200()

#data = pd.read_sql("SELECT * FROM daily where ticker = 'KBC.BR' ORDER BY datum DESC LIMIT 100",con)
#print data
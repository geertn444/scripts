import requests
import pandas as pd

headers = {
       'Content-Type': 'application/json',
       'Authorization' : 'Token 2bd21cbe50ae4406bd096a78cd1c314dbb515b8e'
       }
#requestResponse = requests.get("https://api.tiingo.com/tiingo/daily/ENZL/prices",headers=headers)
requestResponse = requests.get("https://api.tiingo.com/tiingo/daily/ENZL/prices?startDate=2017-06-1&endDate=2017-06-19",headers=headers)
print requestResponse
json_result = requestResponse.json()
print json_result

#result = pd.read_json(json_result,orient='records')
df = pd.DataFrame.from_records(json_result)
df['date']  = pd.to_datetime(df['date'])
df = df.rename(columns={'date': 'Date', 'open': 'Open','adjClose':'Adj Close','volume':'Volume','high':'High','low':'Low'})
a = df.set_index(['Date'])
print a


import xlsxwriter
from openpyxl import load_workbook
import os
import sys
import pandas as pd

#Need to enter the location as first parameter. Location will be prepended to all file names


location = str(sys.argv[1])
print 'Argument List:', location

fname1 = location + " Bezetting RIZIV.xlsx"
fname2 = location + " Gemiddelde leeftijd.xlsx"
fname3 = location + " opnames lft en geslacht.xlsx"
fname4 = location + " Overlijdens en lft.xlsx"
fname5 = location + " Overlijdens en gemiddelde verblijfsduur.xlsx"
fname6 = location + " Definitieve vertrekkers en lft.xlsx"
fname7 = location + " Herkomst opnames.xlsx"


#Creates new workbook (for graphs)
workbook = xlsxwriter.Workbook('graphs ' + location +'.xlsx')

  

if os.path.isfile(fname1):
 print "file does exist at this time, continueing"
 
 #FIRST EXCEL
 wb1 = load_workbook(filename = fname1)
 ws = wb1['Sheet1']
 
 robco = []
 for row in ws.iter_rows('J17:J19'):
         for cell in row:
             robco.append(float(cell.value.replace("%", "")))
 for row in ws.iter_rows('J22:J24'):
         for cell in row:
             robco.append(float(cell.value.replace("%", "")))
 for row in ws.iter_rows('J27:J29'):
         for cell in row:
             robco.append(float(cell.value.replace("%", "")))
 for row in ws.iter_rows('J32:J34'):
         for cell in row:
             robco.append(float(cell.value.replace("%", "")))
 
 
 rvtco = []
 for row in ws.iter_rows('Q17:Q19'):
         for cell in row:
             rvtco.append(float(cell.value.replace("%", "")))
 for row in ws.iter_rows('Q22:Q24'):
         for cell in row:
             rvtco.append(float(cell.value.replace("%", "")))
 for row in ws.iter_rows('Q27:Q29'):
         for cell in row:
             rvtco.append(float(cell.value.replace("%", "")))
 for row in ws.iter_rows('Q32:Q34'):
         for cell in row:
             rvtco.append(float(cell.value.replace("%", "")))
 
 			
 labels = []
 for row in ws.iter_rows('A17:A19'):
         for cell in row:
             labels.append(cell.value)
 for row in ws.iter_rows('A22:A24'):
         for cell in row:
             labels.append(cell.value)
 for row in ws.iter_rows('A27:A29'):
         for cell in row:
             labels.append(cell.value)
 for row in ws.iter_rows('A32:A34'):
         for cell in row:
             labels.append(cell.value)
 
 print labels
 
 #------ per categorie
 
 catrob = []
 catrvt = []
 catrobperc = []
 catrvtperc = []
 #number is variable, starts at 43, ends at end - 4
 for row in range(43, ws.get_highest_row() - 4):
  # Each row in the spreadsheet has data for one census tract.
  getcat = ws['D' + str(row)].value
  if "'" in getcat:
   #('ROB')
   catrob.append(getcat)
   catrobperc.append(float(ws['K' + str(row)].value.replace("%", "")))
  else:
   #('RVT')
   catrvt.append(getcat)
   catrvtperc.append(float(ws['K' + str(row)].value.replace("%", "")))
  
 
 print catrob
 print catrobperc
 
 print catrvt
 print catrvtperc
 
  
 # --- SHEET ONE --- ROB/RVT percentage per month
 worksheet = workbook.add_worksheet('Data1')
 
 # Add the worksheet data to be plotted.
 worksheet.write_column('A1', robco)
 worksheet.write_column('B1', rvtco)
 worksheet.write_column('C1', labels)
 
 
 chart1 = workbook.add_chart({'type': 'column'})
 chart2 = workbook.add_chart({'type': 'column'})
 
 # Add a series to the chart.
 chart1.add_series({
 	'values': '=Data1!$A$1:$A$12',
 	'categories': '=Data1!$C$1:$C$12'
 	})
 chart1.set_title({
     'name':    'Bezettingscoefficient ROB'})
 chart1.set_legend({'position': 'none'})
 chart1.set_size({'width': 720, 'height': 280})
 
 # Add a series to the chart.
 chart2.add_series({
 	'values': '=Data1!$B$1:$B$12',
 	'categories': '=Data1!$C$1:$C$12'
 	})
 chart2.set_title({
     'name':    'Bezettingscoefficient RVT'})
 chart2.set_legend({'position': 'none'})
 chart2.set_size({'width': 720, 'height': 280})
 
 # Insert the chart into the worksheet.
 worksheet.insert_chart('F1', chart1)
 worksheet.insert_chart('F16', chart2)
 
 
 
 # --- SHEET TWO --- ROB/RVT per categorie
 ws2 = workbook.add_worksheet('Data2')
 
 # Add the worksheet data to be plotted.
 ws2.write_column('A1', catrob)
 ws2.write_column('B1', catrobperc)
 ws2.write_column('C1', catrvt)
 ws2.write_column('D1', catrvtperc)
 
 
 chart1 = workbook.add_chart({'type': 'column'})
 chart2 = workbook.add_chart({'type': 'column'})
 
 # Chart one = ROB categories
 chart1.add_series({
 	'values': ['Data2', 0, 1, len(catrob)-1, 1],
 	'categories': ['Data2', 0, 0, len(catrob)-1, 0]
 	})
 chart1.set_title({
     'name':    'Gemiddelde bezetting per categorie'})
 chart1.set_legend({'position': 'none'})
 chart1.set_size({'width': 720, 'height': 280})
 
 # Chart one = ROB categories
 chart2.add_series({
 	'values': ['Data2', 0, 3, len(catrvt)-1, 3],
 	'categories': ['Data2', 0, 2, len(catrvt)-1, 2]
 	})
 chart2.set_title({
     'name':    'Gemiddelde bezetting per categorie'})
 chart2.set_legend({'position': 'none'})
 chart2.set_size({'width': 720, 'height': 280})
 
 
 # Insert the chart into the worksheet.
 ws2.insert_chart('F1', chart1)
 ws2.insert_chart('F16', chart2)
 
 
if os.path.isfile(fname2):
 print "file does exist at this time, continueing"
 #READ EXCEL FILE 2
 wb2 = load_workbook(filename = fname2)
 ws20 = wb2['Sheet1']
 
 manrob = []
 manrvt = []
 vrouwrob = []
 vrouwrvt = []
 for row in ws20.iter_rows('H11:H24'):
         for cell in row:
             manrob.append(float(cell.value))
 for row in ws20.iter_rows('L11:L24'):
         for cell in row:
             manrvt.append(float(cell.value))
 for row in ws20.iter_rows('R11:R24'):
         for cell in row:
             vrouwrob.append(float(cell.value))
 for row in ws20.iter_rows('T11:T24'):
         for cell in row:
             vrouwrvt.append(float(cell.value))
 labels = []
 for row in ws20.iter_rows('A11:A24'):
         for cell in row:
             labels.append(cell.value)
 
 #Gettotals
 totalrob = [ws20['H26'].value,ws20['R26'].value]
 totalrvt = [ws20['L26'].value,ws20['T26'].value]
 
 print manrob
 print manrvt
 print vrouwrob
 print vrouwrvt
 print labels
 print totalrob
 print totalrvt
 
 # --- SHEET3 --- leeftijdscategorien
 ws3 = workbook.add_worksheet('Data3')
 
 # Add the worksheet data to be plotted.
 ws3.write_column('A1', manrob)
 ws3.write_column('B1', manrvt)
 ws3.write_column('C1', vrouwrob)
 ws3.write_column('D1', vrouwrvt)
 ws3.write_column('E1', labels)
 ws3.write_string('F1','Man')
 ws3.write_string('G1','Vrouw')
 ws3.write_column('H1',totalrob)
 ws3.write_column('I1',totalrvt)
 
 chart1 = workbook.add_chart({'type': 'column'})
 chart2 = workbook.add_chart({'type': 'column'})
 chart3 = workbook.add_chart({'type': 'pie'})
 chart4 = workbook.add_chart({'type': 'pie'})
 
 # Chart one = ROB categories
 chart1.add_series({
 	'values': ['Data3', 0, 0, 13, 0],
 	'categories': ['Data3', 0, 4, 13, 4],
 	'name': '=Data3!$F$1'
 	})
 chart1.add_series({
 	'values': ['Data3', 0, 2, 13, 2],
 	'name': '=Data3!$G$1'
 	})
 chart1.set_title({
     'name':    'Gemiddelde leeftijd ROB'})
 chart1.set_legend({'position': 'right'})
 chart1.set_size({'width': 720, 'height': 280})
 
 # Chart one = ROB categories
 chart2.add_series({
 	'values': ['Data3', 0, 1, 13, 1],
 	'categories': ['Data3', 0, 4, 13, 4],
 	'name': '=Data3!$F$1'
 	})
 chart2.add_series({
   'values': ['Data3', 0, 3, 13, 3],
   'name': '=Data3!$G$1'
   })
 chart2.set_title({
     'name':    'Gemiddelde leeftijd RVT'})
 chart2.set_legend({'position': 'right'})
 chart2.set_size({'width': 720, 'height': 280})
 
 
 #PIECHARTS
 chart3.add_series({
 	'values': ['Data3', 0, 7, 1, 7],
 	'categories': ['Data3', 0, 5, 0, 6],
 	'data_labels': {'value': True},
 	'name': 'ROB'
 	})
 
 chart3.set_title({
     'name':    'Totaal ROB'})
 chart3.set_legend({'position': 'right'})
 chart3.set_size({'width': 360, 'height': 280})
 
 chart4.add_series({
 	'values': ['Data3', 0, 8, 1, 8],
 	'categories': ['Data3', 0, 5, 0, 6],
 	'data_labels': {'value': True},
 	'name': 'RVT'
 	})
 
 chart4.set_title({
     'name':    'Totaal RVT'})
 chart4.set_legend({'position': 'right'})
 chart4.set_size({'width': 360, 'height': 280})
 
 
 
 # Insert the chart into the worksheet.
 ws3.insert_chart('F1', chart1)
 ws3.insert_chart('F16', chart2)
 ws3.insert_chart('F32', chart3)
 ws3.insert_chart('M32', chart4)
 
 
 #----------------------------------------------------------------

#READ EXCEL FILE 3: opnames per leeftijd

if os.path.isfile(fname3):
 print "file does exist at this time, continueing"
 wb3 = load_workbook(filename = fname3) 
 ws30 = wb3['Sheet1']

 manage = []
 vrouwage = []
 
 for row in ws30.iter_rows('H8:H21'):
         for cell in row:
             manage.append(float(cell.value))
 for row in ws30.iter_rows('N8:N21'):
         for cell in row:
             vrouwage.append(float(cell.value))
 
 
 labels = []
 for row in ws30.iter_rows('A8:A21'):
         for cell in row:
             labels.append(cell.value)
 
 #Gettotals
 totals = [ws30['H23'].value,ws30['N23'].value]
 
 print manage
 print vrouwage
 print labels
 print totals
 
 
 # --- SHEET4 --- opnames per leeftijd
 ws4 = workbook.add_worksheet('Data4')
 
 # Add the worksheet data to be plotted.
 ws4.write_column('A1', manage)
 ws4.write_column('B1', vrouwage)
 ws4.write_column('C1', labels)
 ws4.write_string('D1','Man')
 ws4.write_string('E1','Vrouw')
 ws4.write_column('F1',totals)
 
 
 chart1 = workbook.add_chart({'type': 'column'})
 chart2 = workbook.add_chart({'type': 'pie'})
 
 chart1.add_series({
 	'values': ['Data4', 0, 0, 13, 0],
 	'categories': ['Data4', 0, 2, 13, 2],
 	'name': '=Data4!$D$1'
 	})
 chart1.add_series({
 	'values': ['Data4', 0, 1, 13, 1],
 	'name': '=Data4!$E$1'
 	})
 chart1.set_title({
     'name':    'Opnames per leeftijd'})
 chart1.set_legend({'position': 'right'})
 chart1.set_size({'width': 720, 'height': 280})
 
 #PIECHART
 chart2.add_series({
 	'values': ['Data4', 0, 5, 1, 5],
 	'categories': ['Data4', 0, 3, 0, 4],
 	'data_labels': {'value': True},
 	'name': 'Pie'
 	})
 
 chart2.set_title({
     'name':    'Totaal aantal opnames'})
 chart2.set_legend({'position': 'right'})
 chart2.set_size({'width': 360, 'height': 280})
 
 
 
 # Insert the chart into the worksheet.
 ws4.insert_chart('F1', chart1)
 ws4.insert_chart('F16', chart2)


#READ EXCEL FILE 4: overlijdens per leeftijd

if os.path.isfile(fname4):
 print "file does exist at this time, continueing"
 wb4 = load_workbook(filename = fname4) 
 ws40 = wb4['Sheet1']

 manage = []
 vrouwage = []
 
 for row in ws40.iter_rows('H8:H21'):
         for cell in row:
             manage.append(float(cell.value))
 for row in ws40.iter_rows('N8:N21'):
         for cell in row:
             vrouwage.append(float(cell.value))
 
 
 labels = []
 for row in ws40.iter_rows('A8:A21'):
         for cell in row:
             labels.append(cell.value)
 
 #Gettotals
 totals = [ws40['H23'].value,ws40['N23'].value]
 
 print manage
 print vrouwage
 print labels
 print totals
 
 
 # --- SHEET5 --- overlijdens per leeftijd
 ws5 = workbook.add_worksheet('Data5')
 
 # Add the worksheet data to be plotted.
 ws5.write_column('A1', manage)
 ws5.write_column('B1', vrouwage)
 ws5.write_column('C1', labels)
 ws5.write_string('D1','Man')
 ws5.write_string('E1','Vrouw')
 ws5.write_column('F1',totals)
 
 
 chart1 = workbook.add_chart({'type': 'column'})
 chart2 = workbook.add_chart({'type': 'pie'})
 
 chart1.add_series({
 	'values': ['Data5', 0, 0, 13, 0],
 	'categories': ['Data5', 0, 2, 13, 2],
 	'name': '=Data5!$D$1'
 	})
 chart1.add_series({
 	'values': ['Data5', 0, 1, 13, 1],
 	'name': '=Data5!$E$1'
 	})
 chart1.set_title({
     'name':    'Overlijdens per leeftijd'})
 chart1.set_legend({'position': 'right'})
 chart1.set_size({'width': 720, 'height': 280})
 
 #PIECHART
 chart2.add_series({
 	'values': ['Data5', 0, 5, 1, 5],
 	'categories': ['Data5', 0, 3, 0, 4],
 	'data_labels': {'value': True},
 	'name': 'Pie'
 	})
 
 chart2.set_title({
     'name':    'Totaal aantal overlijdens'})
 chart2.set_legend({'position': 'right'})
 chart2.set_size({'width': 360, 'height': 280})
 
 
 
 # Insert the chart into the worksheet.
 ws5.insert_chart('F1', chart1)
 ws5.insert_chart('F16', chart2)

#READ EXCEL FILE 5: overlijdenslijst (get only gemiddelde verblijfsduur)

if os.path.isfile(fname5):
 print "file ok, does exist at this time, continueing"
 wb = load_workbook(filename = fname5) 
 ws = wb['Sheet1']
 maxlines = ws.get_highest_row()
 print maxlines
 verblijfsduur = ws.cell(row = maxlines - 3, column = 24).value
 
 # --- SHEET6 --- gemiddelde verblijfsduur
 ws6 = workbook.add_worksheet('Data6')
 
 # Add the worksheet data to be plotted.
 ws6.write_string('A1', 'Gemiddelde verblijfsduur bij overlijden:')
 ws6.write_string('F1', verblijfsduur)
 
 
 
 

 
#READ EXCEL FILE 6: vertrekkers per leeftijd/geslacht

if os.path.isfile(fname6):
 print "file ok, does exist at this time, continueing"
 wb = load_workbook(filename = fname6) 
 ws = wb['Sheet1']

 manage = []
 vrouwage = []
 
 for row in ws.iter_rows('H8:H21'):
         for cell in row:
             manage.append(float(cell.value))
 for row in ws.iter_rows('N8:N21'):
         for cell in row:
             vrouwage.append(float(cell.value))
 
 
 labels = []
 for row in ws.iter_rows('A8:A21'):
         for cell in row:
             labels.append(cell.value)
 
 #Gettotals
 totals = [ws['H23'].value,ws['N23'].value]
 
 print manage
 print vrouwage
 print labels
 print totals
 
 
 # --- SHEET5 --- overlijdens per leeftijd
 ws7 = workbook.add_worksheet('Data7')
 
 # Add the worksheet data to be plotted.
 ws7.write_column('A1', manage)
 ws7.write_column('B1', vrouwage)
 ws7.write_column('C1', labels)
 ws7.write_string('D1','Man')
 ws7.write_string('E1','Vrouw')
 ws7.write_column('F1',totals)
 
 
 chart1 = workbook.add_chart({'type': 'column'})
 chart2 = workbook.add_chart({'type': 'pie'})
 
 chart1.add_series({
 	'values': ['Data7', 0, 0, 13, 0],
 	'categories': ['Data7', 0, 2, 13, 2],
 	'name': '=Data7!$D$1'
 	})
 chart1.add_series({
 	'values': ['Data7', 0, 1, 13, 1],
 	'name': '=Data7!$E$1'
 	})
 chart1.set_title({
     'name':    'Definitieve vertrekken per leeftijd'})
 chart1.set_legend({'position': 'right'})
 chart1.set_size({'width': 720, 'height': 280})
 
 #PIECHART
 chart2.add_series({
 	'values': ['Data7', 0, 5, 1, 5],
 	'categories': ['Data7', 0, 3, 0, 4],
 	'data_labels': {'value': True},
 	'name': 'Pie'
 	})
 
 chart2.set_title({
     'name':    'Totaal aantal def vertrekken'})
 chart2.set_legend({'position': 'right'})
 chart2.set_size({'width': 360, 'height': 280})
 
 
 
 # Insert the chart into the worksheet.
 ws7.insert_chart('F1', chart1)
 ws7.insert_chart('F16', chart2)

 
 
 #READ EXCEL FILE 7: pivot table, herkomst opnames
if os.path.isfile(fname7):
 print "file does exist at this time, continueing"

 wb = load_workbook(filename = fname7)
 ws = wb['Sheet1']
 maxlines = ws.get_highest_row()
 print maxlines
 
 herkomst = []
 excelrange = 'V14:V' + str(maxlines-6)
 print excelrange

 for row in ws.iter_rows(excelrange):
         for cell in row:
             herkomst.append(cell.value)
 print herkomst
 #convert to pandas series
 #trick: make double column for groupby (just the way pandas works)
 df = pd.DataFrame({'A' : herkomst,'B' : herkomst})
 #print df
 #calculate pivottable count
 df = df.groupby('A').count()
 #print df2
 
 #open new file for this one
 #this will not append to file , but erase file completely !!
 writer = pd.ExcelWriter('graphs ' + location +' pivot.xlsx', engine='xlsxwriter')
 df.to_excel(writer, sheet_name='Data1')
 wb = writer.book
 wsp = writer.sheets['Data1']
 chart1 = wb.add_chart({'type': 'column'})
 chart1.add_series({
 	'values': ['Data1', 0, 2, 5, 2],
 	'categories': ['Data1', 0, 1, 5, 1]
 	})
 chart1.set_title({
     'name':    'xxxxx'})
 chart1.set_legend({'position': 'right'})
 chart1.set_size({'width': 720, 'height': 280})
 wsp.insert_chart('F1', chart1)
 
 
 









workbook.close()
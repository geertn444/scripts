import xlsxwriter
from openpyxl import load_workbook
import os

wb2 = load_workbook(filename = 'Binkom Lft en geslacht.xlsx')


f1 = "Bezettingsgraad RIZIV Binkom.xlsx"
if os.path.isfile(f1):
    print "file does exist at this time"

wb1 = load_workbook(filename = f1)
ws = wb1['Sheet1']

robco = []
for row in ws.iter_rows('J17:J19'):
        for cell in row:
            robco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('J22:J24'):
        for cell in row:
            robco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('J27:J29'):
        for cell in row:
            robco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('J32:J34'):
        for cell in row:
            robco.append(float(cell.value.replace("%", "")))


rvtco = []
for row in ws.iter_rows('Q17:Q19'):
        for cell in row:
            rvtco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('Q22:Q24'):
        for cell in row:
            rvtco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('Q27:Q29'):
        for cell in row:
            rvtco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('Q32:Q34'):
        for cell in row:
            rvtco.append(float(cell.value.replace("%", "")))

			
labels = []
for row in ws.iter_rows('A17:A19'):
        for cell in row:
            labels.append(cell.value)
for row in ws.iter_rows('A22:A24'):
        for cell in row:
            labels.append(cell.value)
for row in ws.iter_rows('A27:A29'):
        for cell in row:
            labels.append(cell.value)
for row in ws.iter_rows('A32:A34'):
        for cell in row:
            labels.append(cell.value)

print labels

#------ per categorie

catrob = []
catrvt = []
catrobperc = []
catrvtperc = []
#number is variable, starts at 43, ends at end - 4
for row in range(43, ws.get_highest_row() - 4):
 # Each row in the spreadsheet has data for one census tract.
 getcat = ws['D' + str(row)].value
 if "'" in getcat:
  #('ROB')
  catrob.append(getcat)
  catrobperc.append(float(ws['K' + str(row)].value.replace("%", "")))
 else:
  #('RVT')
  catrvt.append(getcat)
  catrvtperc.append(float(ws['K' + str(row)].value.replace("%", "")))
 

print catrob
print catrobperc

print catrvt
print catrvtperc

#Creates new workbook !!!!
workbook = xlsxwriter.Workbook('graphs.xlsx')



# --- CHART ONE --- ROB/RVT percentage per month
worksheet = workbook.add_worksheet()

# Add the worksheet data to be plotted.
worksheet.write_column('A1', robco)
worksheet.write_column('B1', rvtco)
worksheet.write_column('C1', labels)


chart1 = workbook.add_chart({'type': 'column'})
chart2 = workbook.add_chart({'type': 'column'})

# Add a series to the chart.
chart1.add_series({
	'values': '=Sheet1!$A$1:$A$12',
	'categories': '=Sheet1!$C$1:$C$12'
	})
chart1.set_title({
    'name':    'Bezettingscoefficient ROB'})
chart1.set_legend({'position': 'none'})
chart1.set_size({'width': 720, 'height': 280})

# Add a series to the chart.
chart2.add_series({
	'values': '=Sheet1!$B$1:$B$12',
	'categories': '=Sheet1!$C$1:$C$12'
	})
chart2.set_title({
    'name':    'Bezettingscoefficient RVT'})
chart2.set_legend({'position': 'none'})
chart2.set_size({'width': 720, 'height': 280})

# Insert the chart into the worksheet.
worksheet.insert_chart('F1', chart1)
worksheet.insert_chart('F16', chart2)



# --- CHART TWO --- ROB/RVT per categorie
ws2 = workbook.add_worksheet()

# Add the worksheet data to be plotted.
ws2.write_column('A1', catrob)
ws2.write_column('B1', catrobperc)
ws2.write_column('C1', catrvt)
ws2.write_column('D1', catrvtperc)


chart1 = workbook.add_chart({'type': 'column'})
chart2 = workbook.add_chart({'type': 'column'})

# Chart one = ROB categories
chart1.add_series({
	'values': ['Sheet2', 0, 1, len(catrob)-1, 1],
	'categories': ['Sheet2', 0, 0, len(catrob)-1, 0]
	})
chart1.set_title({
    'name':    'Gemiddelde bezetting per categorie'})
chart1.set_legend({'position': 'none'})
chart1.set_size({'width': 720, 'height': 280})

# Chart one = ROB categories
chart2.add_series({
	'values': ['Sheet2', 0, 3, len(catrvt)-1, 3],
	'categories': ['Sheet2', 0, 2, len(catrvt)-1, 2]
	})
chart2.set_title({
    'name':    'Gemiddelde bezetting per categorie'})
chart2.set_legend({'position': 'none'})
chart2.set_size({'width': 720, 'height': 280})


# Insert the chart into the worksheet.
ws2.insert_chart('F1', chart1)
ws2.insert_chart('F16', chart2)








workbook.close()
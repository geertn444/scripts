import xlsxwriter
from openpyxl import load_workbook

#FIRST EXCEL
wb1 = load_workbook(filename = 'Bezettingsgraad RIZIV Binkom.xlsx')
wb2 = load_workbook(filename = 'Binkom Lft en geslacht.xlsx')
wb3 = load_workbook(filename = 'Binkom opnames per lft.xlsx')
ws = wb1['Sheet1']
ws20 = wb2['Sheet1']
ws30 = wb3['Sheet1']

robco = []
for row in ws.iter_rows('J17:J19'):
        for cell in row:
            robco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('J22:J24'):
        for cell in row:
            robco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('J27:J29'):
        for cell in row:
            robco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('J32:J34'):
        for cell in row:
            robco.append(float(cell.value.replace("%", "")))


rvtco = []
for row in ws.iter_rows('Q17:Q19'):
        for cell in row:
            rvtco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('Q22:Q24'):
        for cell in row:
            rvtco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('Q27:Q29'):
        for cell in row:
            rvtco.append(float(cell.value.replace("%", "")))
for row in ws.iter_rows('Q32:Q34'):
        for cell in row:
            rvtco.append(float(cell.value.replace("%", "")))

			
labels = []
for row in ws.iter_rows('A17:A19'):
        for cell in row:
            labels.append(cell.value)
for row in ws.iter_rows('A22:A24'):
        for cell in row:
            labels.append(cell.value)
for row in ws.iter_rows('A27:A29'):
        for cell in row:
            labels.append(cell.value)
for row in ws.iter_rows('A32:A34'):
        for cell in row:
            labels.append(cell.value)

print labels

#------ per categorie

catrob = []
catrvt = []
catrobperc = []
catrvtperc = []
#number is variable, starts at 43, ends at end - 4
for row in range(43, ws.get_highest_row() - 4):
 # Each row in the spreadsheet has data for one census tract.
 getcat = ws['D' + str(row)].value
 if "'" in getcat:
  #('ROB')
  catrob.append(getcat)
  catrobperc.append(float(ws['K' + str(row)].value.replace("%", "")))
 else:
  #('RVT')
  catrvt.append(getcat)
  catrvtperc.append(float(ws['K' + str(row)].value.replace("%", "")))
 

print catrob
print catrobperc

print catrvt
print catrvtperc

#Creates new workbook !!!!
workbook = xlsxwriter.Workbook('graphs.xlsx')



# --- SHEET ONE --- ROB/RVT percentage per month
worksheet = workbook.add_worksheet()

# Add the worksheet data to be plotted.
worksheet.write_column('A1', robco)
worksheet.write_column('B1', rvtco)
worksheet.write_column('C1', labels)


chart1 = workbook.add_chart({'type': 'column'})
chart2 = workbook.add_chart({'type': 'column'})

# Add a series to the chart.
chart1.add_series({
	'values': '=Sheet1!$A$1:$A$12',
	'categories': '=Sheet1!$C$1:$C$12'
	})
chart1.set_title({
    'name':    'Bezettingscoefficient ROB'})
chart1.set_legend({'position': 'none'})
chart1.set_size({'width': 720, 'height': 280})

# Add a series to the chart.
chart2.add_series({
	'values': '=Sheet1!$B$1:$B$12',
	'categories': '=Sheet1!$C$1:$C$12'
	})
chart2.set_title({
    'name':    'Bezettingscoefficient RVT'})
chart2.set_legend({'position': 'none'})
chart2.set_size({'width': 720, 'height': 280})

# Insert the chart into the worksheet.
worksheet.insert_chart('F1', chart1)
worksheet.insert_chart('F16', chart2)



# --- SHEET TWO --- ROB/RVT per categorie
ws2 = workbook.add_worksheet()

# Add the worksheet data to be plotted.
ws2.write_column('A1', catrob)
ws2.write_column('B1', catrobperc)
ws2.write_column('C1', catrvt)
ws2.write_column('D1', catrvtperc)


chart1 = workbook.add_chart({'type': 'column'})
chart2 = workbook.add_chart({'type': 'column'})

# Chart one = ROB categories
chart1.add_series({
	'values': ['Sheet2', 0, 1, len(catrob)-1, 1],
	'categories': ['Sheet2', 0, 0, len(catrob)-1, 0]
	})
chart1.set_title({
    'name':    'Gemiddelde bezetting per categorie'})
chart1.set_legend({'position': 'none'})
chart1.set_size({'width': 720, 'height': 280})

# Chart one = ROB categories
chart2.add_series({
	'values': ['Sheet2', 0, 3, len(catrvt)-1, 3],
	'categories': ['Sheet2', 0, 2, len(catrvt)-1, 2]
	})
chart2.set_title({
    'name':    'Gemiddelde bezetting per categorie'})
chart2.set_legend({'position': 'none'})
chart2.set_size({'width': 720, 'height': 280})


# Insert the chart into the worksheet.
ws2.insert_chart('F1', chart1)
ws2.insert_chart('F16', chart2)

#READ EXCEL FILE 2

manrob = []
manrvt = []
vrouwrob = []
vrouwrvt = []
for row in ws20.iter_rows('H11:H24'):
        for cell in row:
            manrob.append(float(cell.value))
for row in ws20.iter_rows('L11:L24'):
        for cell in row:
            manrvt.append(float(cell.value))
for row in ws20.iter_rows('R11:R24'):
        for cell in row:
            vrouwrob.append(float(cell.value))
for row in ws20.iter_rows('T11:T24'):
        for cell in row:
            vrouwrvt.append(float(cell.value))
labels = []
for row in ws20.iter_rows('A11:A24'):
        for cell in row:
            labels.append(cell.value)

#Gettotals
totalrob = [ws20['H26'].value,ws20['R26'].value]
totalrvt = [ws20['L26'].value,ws20['T26'].value]

print manrob
print manrvt
print vrouwrob
print vrouwrvt
print labels
print totalrob
print totalrvt

# --- SHEET3 --- leeftijdscategorien
ws3 = workbook.add_worksheet()

# Add the worksheet data to be plotted.
ws3.write_column('A1', manrob)
ws3.write_column('B1', manrvt)
ws3.write_column('C1', vrouwrob)
ws3.write_column('D1', vrouwrvt)
ws3.write_column('E1', labels)
ws3.write_string('F1','Man')
ws3.write_string('G1','Vrouw')
ws3.write_column('H1',totalrob)
ws3.write_column('I1',totalrvt)

chart1 = workbook.add_chart({'type': 'column'})
chart2 = workbook.add_chart({'type': 'column'})
chart3 = workbook.add_chart({'type': 'pie'})
chart4 = workbook.add_chart({'type': 'pie'})

# Chart one = ROB categories
chart1.add_series({
	'values': ['Sheet3', 0, 0, 13, 0],
	'categories': ['Sheet3', 0, 4, 13, 4],
	'name': '=Sheet3!$F$1'
	})
chart1.add_series({
	'values': ['Sheet3', 0, 2, 13, 2],
	'name': '=Sheet3!$G$1'
	})
chart1.set_title({
    'name':    'Gemiddelde leeftijd ROB'})
chart1.set_legend({'position': 'right'})
chart1.set_size({'width': 720, 'height': 280})

# Chart one = ROB categories
chart2.add_series({
	'values': ['Sheet3', 0, 1, 13, 1],
	'categories': ['Sheet3', 0, 4, 13, 4],
	'name': '=Sheet3!$F$1'
	})
chart2.add_series({
  'values': ['Sheet3', 0, 3, 13, 3],
  'name': '=Sheet3!$G$1'
  })
chart2.set_title({
    'name':    'Gemiddelde leeftijd RVT'})
chart2.set_legend({'position': 'right'})
chart2.set_size({'width': 720, 'height': 280})


#PIECHARTS
chart3.add_series({
	'values': ['Sheet3', 0, 7, 1, 7],
	'categories': ['Sheet3', 0, 5, 0, 6],
	'data_labels': {'value': True},
	'name': 'ROB'
	})

chart3.set_title({
    'name':    'Totaal ROB'})
chart3.set_legend({'position': 'right'})
chart3.set_size({'width': 360, 'height': 280})

chart4.add_series({
	'values': ['Sheet3', 0, 8, 1, 8],
	'categories': ['Sheet3', 0, 5, 0, 6],
	'data_labels': {'value': True},
	'name': 'RVT'
	})

chart4.set_title({
    'name':    'Totaal RVT'})
chart4.set_legend({'position': 'right'})
chart4.set_size({'width': 360, 'height': 280})



# Insert the chart into the worksheet.
ws3.insert_chart('F1', chart1)
ws3.insert_chart('F16', chart2)
ws3.insert_chart('F32', chart3)
ws3.insert_chart('M32', chart4)


#----------------------------------------------------------------

#READ EXCEL FILE 3: opnames per leeftijd

manage = []
vrouwage = []

for row in ws30.iter_rows('H8:H21'):
        for cell in row:
            manage.append(float(cell.value))
for row in ws30.iter_rows('N8:N21'):
        for cell in row:
            vrouwage.append(float(cell.value))


labels = []
for row in ws30.iter_rows('A8:A21'):
        for cell in row:
            labels.append(cell.value)

#Gettotals
totals = [ws30['H23'].value,ws30['N23'].value]

print manage
print vrouwage
print labels
print totals


# --- SHEET4 --- opnames per leeftijd
ws4 = workbook.add_worksheet()

# Add the worksheet data to be plotted.
ws4.write_column('A1', manage)
ws4.write_column('B1', vrouwage)
ws4.write_column('C1', labels)
ws4.write_string('D1','Man')
ws4.write_string('E1','Vrouw')
ws4.write_column('F1',totals)


chart1 = workbook.add_chart({'type': 'column'})
chart2 = workbook.add_chart({'type': 'pie'})

chart1.add_series({
	'values': ['Sheet4', 0, 0, 13, 0],
	'categories': ['Sheet4', 0, 2, 13, 2],
	'name': '=Sheet4!$D$1'
	})
chart1.add_series({
	'values': ['Sheet4', 0, 1, 13, 1],
	'name': '=Sheet4!$E$1'
	})
chart1.set_title({
    'name':    'Opnames per leeftijd'})
chart1.set_legend({'position': 'right'})
chart1.set_size({'width': 720, 'height': 280})

#PIECHART
chart2.add_series({
	'values': ['Sheet4', 0, 5, 1, 5],
	'categories': ['Sheet4', 0, 3, 0, 4],
	'data_labels': {'value': True},
	'name': 'Pie'
	})

chart2.set_title({
    'name':    'Totaal aantal opnames'})
chart2.set_legend({'position': 'right'})
chart2.set_size({'width': 360, 'height': 280})



# Insert the chart into the worksheet.
ws4.insert_chart('F1', chart1)
ws4.insert_chart('F16', chart2)


















workbook.close()
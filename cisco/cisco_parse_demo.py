#CISCOCONFPARSE demo script

from ciscoconfparse import CiscoConfParse
from ciscoconfparse import CiscoPassword


def fixed_duplex(obj):
 return obj.has_child_with(r'duplex') and not obj.has_child_with(r'duplex auto')

def fixed_speed(obj):
 return obj.has_child_with(r'speed') and not obj.has_child_with(r'speed auto')

parse = CiscoConfParse("./drendca05z01aa.txt")
parse.append_line('!')
parse.prepend_line('service timestamps log datetime msec localtime show-timezone')

int = parse.find_objects("^interface FastEthernet")
for obj in int: 
 print obj.text
 #add something in interface config
 #NOTE: you need to add spaces yourself
 obj.append_to_family(' !!!')
 
 if fixed_speed(obj):
  print obj.text + "fixed speed"
  #delete something from child config (remove fixed speed)
  obj.delete_children_matching('speed')
  
 if fixed_duplex(obj):
  #delete duplex settings
  obj.delete_children_matching('duplex')
  
lines = parse.find_lines("^interface FastEthernet")
for int_lines in lines:
 print int_lines
 
parse.replace_lines('^interface FastEthernet','interface GigabitEthernet')

trunks = parse.find_parents_w_child("^interface","switchport trunk")
for intf in trunks:
 print intf

notrunks = parse.find_parents_wo_child("^interface","switchport trunk")
for intf in notrunks:
 print intf

console = parse.find_objects(r'password 7')
for obj in console:
 pwd = obj.re_match('password 7 (.*)')
 print pwd
 dp = CiscoPassword()
 print "decrypted:" + dp.decrypt(pwd)
 
#must be called before doing search again
parse.commit()

#write new configuration
parse.save_as('./drendca05z01aa_MODIF.txt')

! global enable
! watch out: all macros are by default enabled. Just MUST limit processing or else config will get replace when phone/switch/router is detected
! workaround: replace all default macros with a "do nothing" macro
macro auto global processing

! limit macro processing on this interface to lightweight-ap
int x/x
 macro auto control device lightweight-ap 

!disable macro processing on this interface
int x/x
 no macro auto processing

! macro to put LWAP in Gigabit speed (and back to standard when shutdown)
! note: i had to use IF (uplink == YES) ELSE
! Using IF (UPLINK == YES) and IF (UPLINK == NO) did not work, the removal part never ran
macro auto execute CISCO_WIRELESS_LIGHTWEIGHT_AP_EVENT  {
if [[ $LINKUP == YES ]]; 
then conf t 
 interface $INTERFACE
 macro description $TRIGGER
 speed auto
 exit
 end
 else conf t
 interface $INTERFACE
 no macro description
 speed auto 10 100
 exit
 end
fi
}


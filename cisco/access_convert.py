#Convert GSK Access C3750 stack to 2960X config
#
#Actions:
#
#Replace FastEthernet -> GigabitEthernet
#Adjust uplink interface indexes
#Remove config of 2th and 3th uplink interface
#Restrict AutoNeg to 10/100, expect on ports with fixed speed applied (not yet: not on Cisco Antenna ports)
#Remove "switch x provision WS-C3750xxx" lines
#Add QOS with service-policy
#Comment out macro lines (away with them)
#Fix banner ^CCC syntaxes
#Fix http(s) service (disable http & https services if not already done so)
#Comment out unsupported features/CLIs
# - "mls qos srr-queue input" lines (only output supported on 2960X)
# - "system mtu routing 1500" (N/A for 2960X)
# - "ip classless" (N/A for 2960X)
# - "control-plane" (N/A for 2960X)
# - "switchport trunk encapsulation dot1q" (N/A for 2960X, dot1q is the only one supported)
#LIMITATIONS: Does not yet support CASCADE switches. Cascade downlink config will be removed !!
#LIMITATIONS: Only 24port and 48 port 3750 switches for the moment
#LIMITATIONS: "monitor session" config might get removed, interface indexes of monitor sessions are not converted (yet)
#LIMITATIONS: The script does no provisioning. The stack must be complete with the same number of switches as before
#LIMITATIONS: Only Access, LPNs not tested. Use at your own risk.



from ciscoconfparse import CiscoConfParse
from ciscoconfparse import CiscoPassword
import sys


#def fixed_duplex(obj):
# return obj.has_child_with(r'duplex') and not obj.has_child_with(r'duplex auto')

#def fixed_speed(obj):
# return obj.has_child_with(r'speed') and not obj.has_child_with(r'speed auto')
 
#def is_sfp_interface(tekst):
#procedure to determine if interface is SFP interface or not (based on interface index)
#needs to take into account switch model (ge/fe) and number of ports (24/48)
#input = "interface line"
#warning there are 24 port switches with 2 SFPs (FE) and some with 4 SFPs (GE) ??
# switchindex = obj.re_match('GigabitEthernet(\d)\/0/')
# print switchindex
# return true

switchnaam = sys.argv[1]
 
parse = CiscoConfParse('./'+switchnaam+'.txt')

#Try to determine parameters of the stack

model = {}
ports = {}
gig = {}

for obj in parse.find_objects('^switch\s\d\sprovision'):
 slot = obj.re_match_typed('^switch\s(\d)\sprovision',result_type=int,default=-1)
 #print str(slot)
 type = obj.re_match_typed('^switch \d provision (\S+)$')
 #print type
 model[slot] = type
 port_num = obj.re_match_typed('^switch \d provision \S+-(\d\d)[ptf][sd]?$',result_type=int,default=-1)
 #print str(port_num)
 ports[slot] = port_num
 #NEEDS BUGFIX DOESNT MATCH OTHER switches as 3750, needs fixing ??
 #POSSIBLE 3750 PROVISION STRINGS
  # nme-xd-24es-1s         provision an EtherSwitch SM with 24+1G interfaces
  # nme-xd-24es-1s-p       provision an EtherSwitch SM with 24+1G interfaces
  # ws-c3750-24fs          provision a Catalyst 3750 switch with 24FX+2G interfaces
  # ws-c3750-24p           provision a Catalyst 3750 switch with 24pwr+2G interfaces
  # ws-c3750-24ts          provision a Catalyst 3750 switch with 24+2G interfaces
  # ws-c3750-48p           provision a Catalyst 3750 switch with 48pwr+4G interfaces
  # ws-c3750-48ts          provision a Catalyst 3750 switch with 48+4G interfaces
  # ws-c3750e-24pd         provision a Catalyst 3750E switch with 24GPwr+2TenG interfaces
  # ws-c3750e-24td         provision a Catalyst 3750E switch with 24G+2TenG interfaces
  # ws-c3750e-48pd         provision a Catalyst 3750E switch with 48GPwr+2TenG interfaces
  # ws-c3750e-48td         provision a Catalyst 3750E switch with 48G+2TenG interfaces
  # ws-c3750g-12s          provision a Catalyst 3750 switch with 12G interfaces
  # ws-c3750g-12s-d        provision a Catalyst 3750 switch with 12G interfaces and DC power supply
  # ws-c3750g-16td         provision a Catalyst 3750 switch with 16G+1XENPAK interfaces
  # ws-c3750g-24ps         provision a Catalyst 3750 switch with 24Gpwr+4G interfaces
  # ws-c3750g-24t          provision a Catalyst 3750 switch with 24GTX interfaces
  # ws-c3750g-24ts         provision a Catalyst 3750 switch with 24GTX+4G interfaces
  # ws-c3750g-24ts-1u      provision a Catalyst 3750 switch with 24GTX+4G interfaces
  # ws-c3750g-24ws-s25/50  provision a Catalyst 3750 switch with 24Gpwr+2G interfaces + Wireless Controller
  # ws-c3750g-48ps         provision a Catalyst 3750 switch with 48Gpwr +4G interfaces
  # ws-c3750g-48ts         provision a Catalyst 3750 switch with 48GTX+4G interfaces
  # ws-c3750v2-24ps        provision a Catalyst 3750V2 switch with 24pwr+2G interfaces
  # ws-c3750v2-24ts        provision a Catalyst 3750V2 switch with 24+2G interfaces
  # ws-c3750v2-48ps        provision a Catalyst 3750V2 switch with 48pwr+4G interfaces
  # ws-c3750v2-48ts        provision a Catalyst 3750V2 switch with 48+4G interfaces
 
 
 g = obj.re_match_typed('^switch \d provision \S+-c3750([ge]?)-')
 #print g
 if (g=='g' or g=='e'):
  cond = True
 else:
  cond = False
 gig[slot] = cond
 

stacklength = len(model)
print "Number of stack switches detected: " + str(stacklength)


#Global changes

#Fix banner ^CCC syntaxes
parse.replace_lines(r'\^C+','^C')

#Comment out incompatible 2960X lines
parse.replace_lines(r'^ip classless$','!ip classless')
parse.replace_lines(r'^system mtu routing 1500$','!system mtu routing 1500')
parse.replace_lines(r'^mls qos srr-queue input',"!mls qos srr-queue input")
parse.replace_lines(r'^control-plane$','!control-plane')
parse.replace_lines(r'^version','!version')
#parse.replace_lines(r'switchport trunk encapsulation dot1q','! switchport trunk encapsulation dot1q')

#Fix HTTP(S) Services
parse.replace_lines(r'^ip http server$','no ip http server')
parse.replace_lines(r'^ip http secure-server','no ip http secure-server')

#Delete Macros
#First remove all lines starting with #
#this should fix the parent-child relationship in macro section

#note: delete_lines did not work (i guess because after deleting a line, the object is reparsed completely and some error happens here (also lots of CPU)
#therefore, better to replace lines with comment line (then line number stays the same)
parse.replace_lines(r"^#","!")
parse.replace_lines(r"^@","!")
#print macros
#parse.delete_lines(r"^#")
parse.commit()

macros = parse.find_objects('macro name')
for mac in macros:
 print "Deleting: " + mac.text
 mac.delete(recurse=True)

 
 #Add QOS Service Policy
 #

parse.insert_before('^interface Null0$', "ip access-list extended ALL-MC")
parse.insert_before('^interface Null0$', " permit ip any 224.0.0.0 31.255.255.255")
parse.insert_before('^interface Null0$', "ip access-list extended STAGING")
parse.insert_before('^interface Null0$', " permit ip any host 239.83.100.109")
parse.insert_before('^interface Null0$', "class-map match-all MC")
parse.insert_before('^interface Null0$', " match access-group name ALL-MC")
parse.insert_before('^interface Null0$', "class-map match-all STAGE")
parse.insert_before('^interface Null0$', " match access-group name STAGING")
parse.insert_before('^interface Null0$', "policy-map END-USER-POLICY")
parse.insert_before('^interface Null0$', " class STAGE")
parse.insert_before('^interface Null0$', "  trust dscp")
parse.insert_before('^interface Null0$', " class MC")
parse.insert_before('^interface Null0$', "  police 1000000 8000 exceed-action drop")
parse.insert_before('^interface Null0$', "!")

 
 
 
#Looping through each switch

for i in range (1,stacklength+1):
 print "Switch "+str(i)+": " + str(ports[i]) + " ports detected"
 


uplinks = parse.find_parents_w_child("^interface GigabitEthernet","description TRK \w\w\wNDCD")
for intf in uplinks:
 print "Detected Uplinks: " + intf
 #print is_sfp_interface(intf)


cascade = parse.find_parents_w_child("^interface GigabitEthernet","description TRK \w\w\wNDCA")
if not cascade:
 print "No Cascades detected."
else:
 for intf in cascade:
  print "Detected Cascades: " + intf

#if (stacklength == 1):
 ##Convert single switch
# print "go"
 
#elif (stacklength == 5):
 ##Remove SFP Gi interface 2 & 3 from config (bug: only when no cascades are detected)
 ##BUG: access switch can be gigabit switch also -> index number changes to 49 & 50
for i in range (1,stacklength+1):
  print "Processing Switch " + str(i)
  
  regex_user_24 = '([1-9]|1[0-9]|2[0-4])$'   #1-24 + match end of line
  regex_user_48 = '([1-9]|1[0-9]|2[0-9]|3[0-9]|4[0-8])$' #1-48 + match end of line
  
  if (ports[i]==24) and (gig[i]==False):
    #delete (Fa) i/0/2 & i/0/3
    del_int = parse.find_objects('GigabitEthernet'+str(i)+'/0/[23]$')
  if (ports[i]==24) and (gig[i]==True):
    #delete (Gi) i/0/26 & i/0/27
    del_int = parse.find_objects('GigabitEthernet'+str(i)+'/0/2[67]$')
  if (ports[i]==48) and (gig[i]==False):
    #delete (Gi) i/0/2 & i/0/3
    del_int = parse.find_objects('GigabitEthernet'+str(i)+'/0/[23]$')
  if (ports[i]==48) and (gig[i]==True):
    #delete (Fa) i/0/50 & i/0/51
    del_int = parse.find_objects('GigabitEthernet'+str(i)+'/0/5[01]$')
  for obj in del_int:
   print "Marked for deletion: " + obj.text
   obj.delete(recurse=True)
  
  parse.commit() #Commit deletion before searching again
  print "Adjusting indexes for uplinks"
  if (gig[i]==False):
   #remaining uplinks are /0/1 & /0/4
   parse.replace_lines('^interface GigabitEthernet'+str(i)+'/0/1$','interface GigabitEthernet'+str(i)+'/0/'+str(ports[i]+1))
   parse.replace_lines('^interface GigabitEthernet'+str(i)+'/0/4$','interface GigabitEthernet'+str(i)+'/0/'+str(ports[i]+2))
  else:
   #note gig switches do not need to be adjusted, their index should already we ok -> wrong, will go from 4 to 2 uplinks !!
   # gi 49 -> maps to 49 (stays the same)
   # gi 52 -> maps to 50 (now) (25 26 27 28 : 28 -> 26
   parse.replace_lines('^interface GigabitEthernet'+str(i)+'/0/'+str(ports[i]+4)+'$','interface GigabitEthernet'+str(i)+'/0/'+str(ports[i]+2))
   
  parse.commit() 
  #Adjust autoneg
  #speed auto 10 100
  #Only for interfaces that do not yet contain 'speed'
  #BUG: only works on FaE, if switches are GIG -> need to be carefull for uplinks (need to be filtered out)
  if (gig[i]==False):
   autoneg = parse.find_objects_wo_child('^interface FastEthernet'+str(i)+'/0/','speed',ignore_ws=True)
  else: #gigabit switch
   if (ports[i]==48):
    #print '^interface GigabitEthernet'+str(i)+'/0/'+regex_user_48
    autoneg = parse.find_objects_wo_child('^interface GigabitEthernet'+str(i)+'/0/'+regex_user_48,'speed',ignore_ws=True)
   else: #24 port
    autoneg = parse.find_objects_wo_child('^interface GigabitEthernet'+str(i)+'/0/'+regex_user_24,'speed',ignore_ws=True)
  
  for intf in autoneg:
   print "Candidates for autoneg adjust: "+ intf.text
   intf.append_to_family(' speed auto 10 100')

  #Add IPV6 config to end-user interfaces (add to all end-user-interfaces)
  #ipv6 nd raguard
  #ipv6 dhcp guard
  #Add QOS
  #service-policy input END-USER-POLICY (add to all end-user-interfaces)
  #BUG: uplink indexes are not yet converted, so GiEth1/0/1 gets config below and is then changed to Gi1/0/49
  if (ports[i]==48):
   addipv6 = parse.find_objects('Ethernet'+str(i)+'/0/'+regex_user_48)  
  else: #24
   addipv6 = parse.find_objects('Ethernet'+str(i)+'/0/'+regex_user_24)  
  for intf in addipv6:
   intf.append_to_family(' ipv6 nd raguard')
   intf.append_to_family(' ipv6 dhcp guard')
   intf.append_to_family(' service-policy input END-USER-POLICY')

#else:
#  print "Not yet supported"
 
parse.commit()
#Convert remaining lines 
parse.replace_lines(r'^interface FastEthernet','interface GigabitEthernet')

#remove old switch types
#BUG: prefer replace_lines with comment, delete_lines tends to be very unreliable
#parse.delete_lines('^switch \d provision')
parse.replace_lines(r'^switch (\d) provision',r"!switch \1 provision")
 
#must be called before doing search again
parse.commit()

#write new configuration
parse.save_as('./MODIF_'+switchnaam+'.txt')

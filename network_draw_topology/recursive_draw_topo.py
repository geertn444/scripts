# Goal is to draw a topology drawing of a network (physical connections) when just a list of IPs is given
# 1) connections will be gathered via a network library with specific procedure that returns CDP neighbors of switches
# 2) We will then use python graph theory library to draw a topology diagram

# Recursive Version
# The python script needs to start with one node and then goes to all other CDP discovered nodes.
# You can define a filter which limits nodes being discovered

import matplotlib.pyplot as plt
import networkx as nx
import mypwd as pwd
import switch as sw
import socket
import re

ignore_phones = True

def myfilter(arg):
 #return len(arg)==14 and re.match('^WAVNDC.21',arg.upper())
 return len(arg)==14 and re.match('^GODND.*',arg.upper())
 
#ciscophone filter
def phonefilter(arg):
 return not re.match('^SEP',arg.upper())

aplookuptable = {
'1242AG' : 'a/g',
'1242': 'b/g',
'2702': 'b/g/n/a/an/ac',
'2602': 'b/g/n/a/an',
'1702': 'b/g/n/a/an/ac',
'1602': 'b/g/n/a/an'
} 
 
# procedure to lookup frequenciebands depending on AP model at GSK
def lookup_AP_frequencies(cdpplatform):
 #extract model
 #example: cisco AIR-CAP2602I-E-K9
 print cdpplatform.upper()
 try:
  model = re.search('.*AIR-\wAP(.*)[IE]-\w-.*',cdpplatform.upper())
  print model.group(1)
 except:
  return 'unknown'
 model = model.group(1)
 # aplookuptable[]
 try:
  x = aplookuptable[model]
 # catch not in there
 except:
  return 'unknown'
 return x
 

def discover(node):
 # add node, only if not already there
 if (node in G.nodes()):
  if G.node[node]['done'] == True:
   print "Already in list and already done. Skipping."
   return
 else:
 #new node
  print "Adding node: " + node
  G.add_node(node)
  G.node[node]['done'] = False
 

 #discover neighbors
 #print G.nodes(data=True)
 print "Discovering: " + node

 #try to dns resolve node
 try:
  ip = socket.gethostbyname(node)
 except socket.gaierror:
  print ('Cannot resolve DNS. Skipping')
  return
  
 print ip
 h = {'device_type': 'cisco_ios','ip':ip,'username':username,'password':password,'secret':password}
 core = sw.Switch(h)
 cdpreturn = core.GetCDPNeighbors()
 
 #catch error or empty return
 if cdpreturn==None:
  return
 
 
 result = cdpreturn['neighbors']
 properties = cdpreturn['properties']


 #neighbors contains names which we want to include in the graph, but no necessarily do ssh recursion on  (for example: include access points in final graph (but don't iterate on access points) and exclude SIP phones)
 #keep all CDP neighbors, except Cisco phones ("SEP...")
 neighbors = filter(phonefilter,result)
 
 #filter result with regex
 #result contains the devices for which we want to do further recursion, so they should filter access/distribution/core switches (but no ip phones or access points)
 result = filter (myfilter,result)
 
 #if ignore_phones:
 # result = filter (phonefilter,result)
 
 print "CDP Neighbors: " + str(neighbors)

 
 # add connections
 for neighbor in neighbors:
  # add connection
  # BUG REPORT: adding an edge with an unknown neighbor will add the neighbor as node, but without attributes
  if not (neighbor in G.nodes()):
   G.add_node(neighbor)
   G.node[neighbor]['done'] = False
   if len(neighbor)==14 and re.match('^...ND.*',neighbor.upper()):
    G.node[neighbor]['nodetype'] = 'switch'
   else:
    #try to determine if node has a CDP Platforom attribute
    try:
     cdpplatform = properties[neighbor]['Platform']
    except:
     print ('No platfrom information for node '.neighbor)
     return
    if 'AIR' in cdpplatform:
     #node is an AP
     G.node[neighbor]['nodetype'] = 'AP'
     G.node[neighbor]['capabilities'] = lookup_AP_frequencies(cdpplatform)
    
   
  # add connection, but we are sure attributes are defined now
  G.add_edge(node,neighbor, linktype='physical') 
 
 
 #mark this node as done
 print "Marking " + node + " as done."
 G.node[node]['done'] = True

 #print "CDP Neighbors: " + str(result)
 #loop through results
 for discovered in result:
  print "Next: "+ discovered
  discover(discovered)
 

#GLOBALS
username = "gyn94629"
password = pwd.pwd


# This is the seed, should be asked via de command line in next version
# We should also implement a regex filter to limit discovery scope (for example only discover "WAV*" hostnames
device = 'GODNDCD08ZM1AA'

#testentry
#device = 'RIX-CCXX-01281'

# The graph will be the global database
# initialize graph theory graph
G=nx.Graph()



discover(device)
 
 
for node in G.nodes():
 #generate attributes for standard L3 naming (buggy: weak matching, only on lenght = dangarous if an AP exists with 14 characters)
 if len(node)==14 and re.match('^...ND.*',node.upper()):
  G.node[node]['nodetype'] = 'switch'   # adds switchtype to nodes that don't already have one
  G.node[node]['site'] = node[:3]
  G.node[node]['building'] = node[7:9]
  G.node[node]['floor'] = node[10:12]
 else:
  #site/building/floor should be copied from parent
  if len(G.neighbors(node)) == 1:
   parent = G.neighbors(node)[0]
 #print G.node[node]
 
 
 
 
 
 

print("Nodes of graph: ")
print(G.nodes())
print("Edges of graph: ")
print(G.edges())


nx.draw(G,with_labels=True)
plt.savefig("simple_path.png") # save as png
nx.write_pajek(G, "network.net") # write pajek network file
nx.write_gexf(G, "network.gexf") # write gephi network file
plt.show()




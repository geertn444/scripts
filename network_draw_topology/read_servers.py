#Read Excel from file
#Resolve Servers
#Query subnet with Infoblox
#Create graph
# Geert Nijs - 2017


import mypwd
from InfobloxAPIPythonmaster import infoblox as infoblox

from openpyxl import load_workbook
import xlsxwriter

import socket

import matplotlib.pyplot as plt
import networkx as nx



username = "gyn94629"
password = mypwd.pwd

# initialize graph theory graph
G=nx.Graph()

iba_api = infoblox.Infoblox('10.102.79.18', username, password, '1.6', 'default', 'default')

#try:
#    ips = iba_api.get_network_by_ip('10.100.96.233')
#    print ips
#    #for ip in ips:
#    # getinfo = iba_api.get_host_extattrs(ip)
#    # print getinfo
#except Exception as e:
#    print e
    
#FIRST EXCEL


#BUG: GEPHI has an import bug (or a file open bug)
# THEREFORE, the script will not generate an GEFX file anymore, but a normal EXCEL file (which you need to import into the dataastore of gephi yourself)

#Creates new workbook (for graphs)
workbook = xlsxwriter.Workbook('gephi-servers.xlsx')

worksheet = workbook.add_worksheet('nodes')

n_name = ['Id']
n_label = ['Label']
n_type = ['nodetype']
n_site = ['site']


# Add the worksheet data to be plotted.
#worksheet.write_column('A1', robco)
#worksheet.write_column('B1', rvtco)
#worksheet.write_column('C1', labels)


fname1 = "XIT_Servers_GIO.xlsx"
servernamecolumn = 'A'
descriptioncolumn = 'D'

wb1 = load_workbook(filename = fname1)
ws = wb1['Sheet1']

for row in range(2, 15):
#for row in range(2, ws.get_highest_row() + 1):
 # Each row in the spreadsheet has data for one census tract.
 servername = ws[servernamecolumn + str(row)].value
 sitecode = servername[:3]
 desc = ws[descriptioncolumn + str(row)].value
 print servername
  
 #Try to resolve
 try:
  ip = socket.gethostbyname(servername)
 except socket.gaierror:
  print ('Cannot resolve DNS. Skipping')
  continue
 print ip # example: 10.100.69.251
 
 # Query subnet info from Infoblox
 
 try:
  ipsubnet = iba_api.get_network_by_ip(ip)
  print ipsubnet
 except Exception as e:
  print "Could not query subnet:" , e
  continue
 
 #G.add_node(ipsubnet, nodetype = 'subnet')
 n_name.append(ipsubnet)
 n_label.append(ipsubnet)
 n_type.append('subnet')
 n_site.append('')
 
 #G.add_node(servername, nodetype = 'server',site = sitecode)
 n_name.append(servername)
 n_label.append(servername)
 n_type.append('server')
 n_site.append(sitecode)
 
 #G.add_edge(ipsubnet,servername, linktype='serverlink')
 

nx.draw(G,with_labels=True)

worksheet.write_column('A1', n_name)
worksheet.write_column('B1', n_label)
worksheet.write_column('C1', n_type)
worksheet.write_column('D1', n_site)



#plt.savefig("simple_path.png") # save as png
#nx.write_pajek(G, "servers.net") # write pajek network file
#nx.write_gexf(G, "servers.gexf") # write pajek network file




plt.show()
 
  




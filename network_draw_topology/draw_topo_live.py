# Goal is to draw a topology drawing of a network (physical connections) when just a list of IPs is given
# 1) connections will be gathered via a network library with specific procedure that returns CDP neighbors of switches
# 2) We will then use python graph theory library to draw a topology diagram

#
# This script will make the graph dynamically live in  Gephi and requires Gephi to be open and a clean workspace0
#


import matplotlib.pyplot as plt
import networkx as nx
import mypwd as pwd
import switch as sw

import pygephi


#GLOBALS
username = "gyn94629"
password = pwd.pwd

lpnwav = {
'WAVNDSE30Z00BA':'10.103.60.2',
'WAVNDSE36ZM1AA':'10.103.60.18',
'WAVNDSE30Z02AB':'10.103.60.34',
'WAVNDSE26Z01AA':'10.100.188.18',
'WAVNDSE31ZM2BA':'10.107.60.3',
'WAVNDSE21B04AA':'10.100.252.2',
'WAVNDSE21B02AB':'10.100.252.34',
'WAVNDSE30Z01AA':'10.103.60.4',
'WAVNDSE30Z02AA':'10.103.60.5',
'WAVNDSE26Z01BA':'10.100.188.19',
'WAVNDSE31Z01AA':'10.107.60.2',
'WAVNDSE31ZM1AA':'10.107.60.4',
'WAVNDSE31Z04BA':'10.107.60.5',
'WAVNDSE31Z04AA':'10.107.60.6',
'WAVNDSE31Z01BA':'10.107.60.7',
'WAVNDSE36ZM1BA':'10.103.60.19',
'WAVNDSE36ZM1CA':'10.103.60.20',
'WAVNDSE36Z02AA':'10.103.60.21',
'WAVNDSE36Z02BA':'10.103.60.22',
'WAVNDSE21B00AA':'10.100.252.35',
'WAVNDSE21B04AB':'10.100.252.36',
'WAVNDSE21C04AC':'10.100.252.37',
'WAVNDSE21FM1AB':'10.100.252.38',
'WAVNDSE21D01AA':'10.100.252.39',
'WAVNDSE21C00AB':'10.100.252.40',
'WAVNDSE21F02AA':'10.100.252.3',
'WAVNDSE21C04AB':'10.100.252.4',
'WAVNDSE21C00AA':'10.100.252.5',
'WAVNDSE21C02AA':'10.100.252.6',
'WAVNDSE21B02AC':'10.100.252.7',
'WAVNDSE48ZM1AA':'10.107.60.18',
'WAVNDSE49ZM2AA':'10.107.60.34',
'WAVNDSE58Z00AA':'10.107.60.50',
'WAVNDSE48ZM1BA':'10.107.60.19',
'WAVNDSE48Z02CA':'10.107.60.23',
'WAVNDSE49Z03CA':'10.107.60.37',
'WAVNDSE49Z03AA':'10.107.60.35',
'WAVNDSE16Z02BA':'10.100.124.2',
'WAVNDSE21FM1AA':'10.100.252.18',
'WAVNDSE21C04AA':'10.100.252.19',
'WAVNDSE27ZM1AA':'10.100.188.2',
'WAVNDSE27ZM1BA':'10.100.188.3',
'WAVNDSE18Z00BA':'10.100.188.34',
'WAVNDSE02Z01BA':'10.100.124.34',
'WAVNDSE50Z01AA':'10.100.124.82',
'WAVNDSE17Z00AA':'10.100.124.66',
'WAVNDSE10Z01AA':'10.100.124.50',
'WAVNDSE48ZM1CA':'10.107.60.20',
'WAVNDSE48Z02BA':'10.107.60.22',
'WAVNDSE49Z03BA':'10.107.60.36',
'WAVNDSE21A02AA':'10.100.252.21',
'WAVNDSE21A00AA':'10.100.252.22',
'WAVNDSE48Z02AA':'10.107.60.21',
'WAVNDSE16Z02AA':'10.100.124.5',
'WAVNDSE16Z01BB':'10.100.124.4',
'WAVNDSE16Z01BA':'10.100.124.3',
'WAVNDSE24Z00AA':'10.100.188.50',
'WAVNDSE21A04AA':'10.100.252.20',
'WAVNDSE06Z01AA':'10.100.124.35',
'WAVNDSE02Z00AA':'10.100.124.36',
'WAVNDSE10Z01BB':'10.100.124.51',
'WAVNDSE21C02AB':'10.100.252.24',
'WAVNDSE10Z01AB':'10.100.188.51',
'WAVNDSE27ZM1AB':'10.100.188.53',
'WAVNDSE21B02BA':'10.100.252.23',
'WAVNDSE10Z01BA':'10.100.188.54',
'WAVNDSE27ZM1BB':'10.100.188.52'
}

core_and_distri = {
'WAVNDCC01Z01BA':'10.96.8.1',
'WAVNDCC18ZM1AA':'10.96.8.2',
'RIXNDCC32200AA':'10.96.0.1',
'RIXNDCC43N00AA':'10.96.0.2',
'RIXNDCD16Z03AA':'10.96.0.3',
'RIXNDCD43N00AA':'10.96.0.4',
'RIXNDCD50Z01AA':'10.96.0.5',
'RIXNDCD59Z03AA':'10.96.0.6',
'RIXNDCD32Z00CA':'10.96.0.7',
'RIXNDCD77Z01AA':'10.96.0.8',
'WAVNDCD01Z01BA':'10.96.8.3',
'WAVNDCD18ZM1AA':'10.96.8.4',
'WAVNDCD01Z01BB':'10.96.8.5',
'WAVNDCD18ZM1AB':'10.96.8.6',
'WAVNDCD21DM1AA':'10.96.8.7',
'WAVNDCD21F04BA':'10.96.8.8',
'WAVNDCD35Z00AA':'10.96.8.9',
'WAVNDCD32Z00AA':'10.96.8.10',
'WAVNDCD23BM1BA':'10.96.8.11',
'WAVNDCD23EM1AA':'10.96.8.12',
'WAVNDCD30Z00AA':'10.96.8.13',
'WAVNDCD36ZM1AA':'10.96.8.14',
'WAVNDCD31ZM2BA':'10.96.8.15',
'WAVNDCD49ZM1AA':'10.96.8.16',
'MSGNDCD11Z02AA':'10.96.88.1',
'APLNDCD98Z00AA':'10.96.76.3',
'APLNDCD99Z01AA':'10.96.76.4'
}




devices = {
'WAVNDCC01Z01BA':'10.96.8.1',
'WAVNDCC18ZM1AA':'10.96.8.2',
'RIXNDCC32200AA':'10.96.0.1',
'RIXNDCC43N00AA':'10.96.0.2',
'APLNDCD99Z01AA':'10.96.76.4',
'APLNDCD98Z00AA':'10.96.76.3',
'WAVNDCD18ZM1AA':'10.96.8.4',
'WAVNDCD01Z01BA':'10.96.8.3',
}

#run for lpnwav
devices = core_and_distri


# initialize graph theory graph
#G=nx.Graph()

G = pygephi.GephiClient('http://localhost:8080/workspace1', autoflush=True)
G.clean()

counter = 0


#otherwise, you don't see a dot
node_attributes = {"size":10}

for dev in devices.keys():
 print dev
 #print devices[dev]
 h = {'device_type': 'cisco_ios','ip':devices[dev],'username':username,'password':password,'secret':password}
 core = sw.Switch(h)
 result = core.GetCDPNeighbors()
 print result
 
 # add node
 G.add_node(dev,**node_attributes) 
 
 
 
 # add connections
 for neighbor in result:
  # add connection
  G.add_edge(counter,dev,neighbor,directed=False)
  counter = counter + 1
  
 
for node in G.nodes():
 #generate attributes for standard L3 naming
 if len(node)==14:
  G.node[node]['site'] = node[:3]
  G.node[node]['building'] = node[7:9]
  G.node[node]['floor'] = node[10:12]
 print G.node[node]
 
 


print("Nodes of graph: ")
print(G.nodes())
print("Edges of graph: ")
print(G.edges())


# adding a list of edges - DEMO
#G.add_edges_from([("a","c"),("c","d"), ("a",1), (1,"d"), ("a",2)])

nx.draw(G,with_labels=True)
plt.savefig("simple_path.png") # save as png
nx.write_pajek(G, "network.net") # write pajek network file
nx.write_gexf(G, "network.gexf") # write pajek network file
plt.show()




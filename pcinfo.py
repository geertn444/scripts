import active_directory
import sys
import socket
import re

def is_valid_ip(ip):
    m = re.match(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", ip)
    return bool(m) and all(map(lambda n: 0 <= int(n) <= 255, m.groups()))

  
if hasattr(socket, 'setdefaulttimeout'):
 # Set the default timeout on sockets to 3 seconds
 socket.setdefaulttimeout(3)

def get_hostname(ip):
 return socket.gethostbyaddr(ip)[0]
 
def get_mudid(hostname):
 #returns the mud id of the last logged on user on hostname
 #need to catch errors
 bestand = r'\\wavoffsrvd01\servicedesk$\Machines\Machines\\' + hostname + ".txt"
 #print bestand
 #need to catch multiline bestand (is possible)
 x = open(bestand)
 for lijn in x:
  #print lijn
  lastlijn = lijn
 return lastlijn.rstrip('\r\n')

 
f = sys.stdin
if len(sys.argv) > 1:
    #f = open(sys.argv[1]) #don't open as file, just take it as ip address or hostname
    f = [sys.argv[1]]
    #print f

    

#MSdomain = active_directory.AD_object ("LDAP://019d.mgd.msft.net")


    
for line in f:
 line = line.rstrip('\r\n')
 if is_valid_ip(line):
  line = get_hostname(line)
 
 line = line.replace(".bio.corpnet1.com","")
 line = line.replace(".BIO.CORPNET1.COM","")
 #print line
 computer = active_directory.find_computer (line)
 print '{0: <16}: {1: <25}: {2: <16}'.format(line,computer.operatingSystem,get_mudid(line))
 

#standard test, non-compiled version
#template is read from a file

from Cheetah.Template import Template

class Client:
 def __init__(self,firstname,lastname):
  self.firstname = firstname
  self.lastname = lastname

 
c1 = Client("Geert","Nijs")
c2 = Client("Nathalie","Verwimp")

#print c1.firstname

clients = [c1,c2]


nameSpace = {'title':'Hello','contents': 'Body',"clients":clients}
t = Template (file="table_temp.tmpl",searchList=[nameSpace])

print t


#now testing with the compiled version of template
#advantages: template can now be accessed as an object itself

from table_temp import table_temp

class Client:
 def __init__(self,firstname,lastname):
  self.firstname = firstname
  self.lastname = lastname

 
c1 = Client("Geert","Nijs")
c2 = Client("Nathalie","Verwimp")

#print c1.firstname

clients = [c1,c2]

t = table_temp()
t.title = 'Hello'
t.contents = 'Body'
t.clients = clients


print t


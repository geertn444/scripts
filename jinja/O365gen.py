import jinja2;
import datetime;
import os;

my_list = [];

for filename in os.listdir('./'):
        if filename.endswith(".txt"):
            my_list.append([filename,filename.rsplit('.', 1)[0]])




templateLoader = jinja2.FileSystemLoader( searchpath="./")
templateEnv = jinja2.Environment( loader=templateLoader )
TEMPLATE_FILE = "main.html"
template = templateEnv.get_template( TEMPLATE_FILE )
template.globals['now'] = datetime.datetime.utcnow
outputText = template.render(items=my_list) # this is where to put args to the template renderer

print outputText

#Python script to convert O365 to Cisco ACL

import os
import re
import sys


subnets = {
 '/32': '255.255.255.255',
 '/31': '255.255.255.254',
 '/30': '255.255.255.252',
 '/29': '255.255.255.248',
 '/28': '255.255.255.224',
 '/27': '255.255.255.192',
 '/26': '255.255.255.144',
 '/25': '255.255.255.128',
 '/24': '255.255.255.0',
 '/23': '255.255.254.0',
 '/22': '255.255.252.0',
 '/21': '255.255.248.0',
 '/20': '255.255.224.0',
 '/19': '255.255.192.0',
 '/18': '255.255.144.0',
 '/17': '255.255.128.0',
 '/16': '255.255.0.0',
 '/15': '255.254.0.0',
 '/14': '255.252.0.0',
 '/13': '255.248.0.0',
 '/12': '255.224.0.0',
 '/11': '255.192.0.0',
 '/10': '255.144.0.0',
 '/9': '255.128.0.0',
 '/8': '255.0.0.0',
 '/7': '254.0.0.0',
 '/6': '252.0.0.0',
 '/5': '248.0.0.0',
 '/4': '224.0.0.0',
 '/3': '192.0.0.0',
 '/2': '144.0.0.0',
 '/1': '128.0.0.0',
 '/0': '0.0.0.0' }
 
isubnets = {
 '/32': '0.0.0.0',
 '/31': '0.0.0.1',
 '/30': '0.0.0.3',
 '/29': '0.0.0.7',
 '/28': '0.0.0.15',
 '/27': '0.0.0.31',
 '/26': '0.0.0.63',
 '/25': '0.0.0.127',
 '/24': '0.0.0.255',
 '/23': '0.0.1.255',
 '/22': '0.0.3.255',
 '/21': '0.0.7.255',
 '/20': '0.0.15.255',
 '/19': '0.0.31.255',
 '/18': '0.0.63.255',
 '/17': '0.0.127.255',
 '/16': '0.0.255.255',
 '/15': '0.1.255.255',
 '/14': '0.3.255.255',
 '/13': '0.7.255.255',
 '/12': '0.15.255.255',
 '/11': '0.31.255.255',
 '/10': '0.63.255.255',
 '/9': '0.127.255.255',
 '/8': '0.255.255.255',
 '/7': '1.255.255.255',
 '/6': '3.255.255.255',
 '/5': '7.255.255.255',
 '/4': '15.255.255.255',
 '/3': '31.255.255.255',
 '/2': '63.255.255.255',
 '/1': '127.255.255.255',
 '/0': '255.255.255.255' }
 



for filename in os.listdir('./'):
        if filename.endswith(".xml"):
            name = filename.rsplit('.', 1)[0]
            result = name + ".acl.txt"
            outfile = open(result,"w")
            Oxml = open(filename, "r")

            start = False

            for line in Oxml:
                match_object = re.search(r'<addresslist type="IPv4">', line)
                eos = re.search(r'</addresslist>',line)
                line = line.rstrip()
                if eos:
                 #stop section
                 start = False
                 outfile.write(line+'\n')
                 continue
                
                if match_object:
                 #start section
                 start = True
                 outfile.write(line+'\n')
                 continue
                 
                if start:
                  #convert
                  #permit ip 193.75.228.0 0.0.0.255 any
                  overeen = re.search(r'<address>(.*)(\/.*)</address>',line)
                  result = "permit ip " + overeen.group(1) + " " + isubnets[overeen.group(2)] + " any"
                  outfile.write (result+'\n')
                else:
                 outfile.write(line+'\n')
            outfile.close
            Oxml.close
      


     

      
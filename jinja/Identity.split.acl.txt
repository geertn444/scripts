  <product name="Identity">
    <addresslist type="URL">
      <address>*.adhybridhealth.azure.com</address>
      <address>*.blob.core.windows.net</address>
      <address>*.cloudapp.net</address>
      <address>*.microsoft.com</address>
      <address>*.microsoftonline.com</address>
      <address>*.microsoftonline-p.com</address>
      <address>*.microsoftonline-p.net</address>
      <address>*.msecnd.net</address>
      <address>*.portal.cloudappsecurity.com</address>
      <address>*.queue.core.windows.net</address>
      <address>*.servicebus.windows.net</address>
      <address>*.table.core.windows.net</address>
      <address>*.windows.net</address>
      <address>account.activedirectory.windowsazure.com</address>
      <address>accounts.accesscontrol.windows.net</address>
      <address>adminwebservice.microsoftonline.com</address>
      <address>api.login.microsoftonline.com</address>
      <address>ccs.login.microsoftonline.com</address>
      <address>ccs-sdf.login.microsoftonline.com</address>
      <address>clientconfig.microsoftonline-p.net</address>
      <address>device.login.microsoftonline.com</address>
      <address>enterpriseregistration.windows.net</address>
      <address>equivio.office.com</address>
      <address>equivioprod*.cloudapp.net</address>
      <address>graph.microsoft.com</address>
      <address>hip.microsoftonline-p.net</address>
      <address>hipservice.microsoftonline.com</address>
      <address>login.microsoft.com</address>
      <address>login.microsoftonline.com</address>
      <address>logincert.microsoftonline.com</address>
      <address>loginex.microsoftonline.com</address>
      <address>login-us.microsoftonline.com</address>
      <address>login.microsoftonline-p.com</address>
      <address>login.windows.net</address>
      <address>management.azure.com</address>
      <address>mscrl.microsoft.com</address>
      <address>nexus.microsoftonline-p.com</address>
      <address>office365servicehealthcommunications.cloudapp.net</address>
      <address>office365zoom.cloudapp.net</address>
      <address>policykeyservice.dc.ad.msft.net</address>
      <address>provisioningapi.microsoftonline.com</address>
      <address>secure.aadcdn.microsoftonline-p.com</address>
      <address>stamp2.login.microsoftonline.com</address>
      <address>zoom-cs-prod*.cloudapp.net</address>
    </addresslist>
    <addresslist type="IPv4">
permit ip 13.67.50.224 0.0.0.7 any
permit ip 13.71.201.64 0.0.0.63 any
permit ip 13.106.4.128 0.0.0.127 any
permit ip 13.75.48.16 0.0.0.7 any
permit ip 13.75.80.16 0.0.0.7 any
permit ip 13.106.56.0 0.0.0.127 any
permit ip 20.190.128.0 0.0.63.255 any
permit ip 23.100.16.168 0.0.0.7 any
permit ip 23.100.32.136 0.0.0.7 any
permit ip 23.100.64.24 0.0.0.7 any
permit ip 23.100.72.32 0.0.0.7 any
permit ip 23.100.80.64 0.0.0.7 any
permit ip 23.100.88.32 0.0.0.7 any
permit ip 23.100.101.112 0.0.0.15 any
permit ip 23.100.104.16 0.0.0.15 any
permit ip 23.100.112.64 0.0.0.7 any
permit ip 23.100.120.64 0.0.0.7 any
permit ip 23.101.5.104 0.0.0.7 any
permit ip 23.101.144.136 0.0.0.7 any
permit ip 23.101.165.168 0.0.0.7 any
permit ip 23.101.181.128 0.0.0.7 any
permit ip 23.101.210.24 0.0.0.7 any
permit ip 23.101.222.240 0.0.0.15 any
permit ip 23.101.224.16 0.0.0.7 any
permit ip 23.101.226.16 0.0.0.15 any
permit ip 40.112.64.16 0.0.0.15 any
permit ip 40.113.192.16 0.0.0.7 any
permit ip 40.114.120.16 0.0.0.7 any
permit ip 40.115.152.16 0.0.0.15 any
permit ip 40.127.67.24 0.0.0.7 any
permit ip 40.126.0.0 0.0.63.255 any
permit ip 52.125.0.0 0.0.127.255 any
permit ip 52.172.144.16 0.0.0.15 any
permit ip 65.52.1.16 0.0.0.7 any
permit ip 65.52.193.136 0.0.0.7 any
permit ip 65.54.170.128 0.0.0.127 any
permit ip 70.37.128.0 0.0.1.255 any
permit ip 104.40.240.48 0.0.0.15 any
permit ip 104.41.13.120 0.0.0.7 any
permit ip 104.41.216.16 0.0.0.15 any
permit ip 104.42.72.16 0.0.0.7 any
permit ip 104.43.208.16 0.0.0.7 any
permit ip 104.43.240.16 0.0.0.7 any
permit ip 104.44.218.128 0.0.0.127 any
permit ip 104.44.254.128 0.0.0.127 any
permit ip 104.44.255.0 0.0.0.127 any
permit ip 104.45.0.16 0.0.0.15 any
permit ip 104.45.208.104 0.0.0.7 any
permit ip 104.46.112.8 0.0.0.7 any
permit ip 104.46.224.64 0.0.0.15 any
permit ip 104.209.144.16 0.0.0.7 any
permit ip 104.210.48.8 0.0.0.7 any
permit ip 104.210.83.160 0.0.0.7 any
permit ip 104.210.208.16 0.0.0.7 any
permit ip 104.211.16.16 0.0.0.7 any
permit ip 104.211.48.16 0.0.0.7 any
permit ip 104.211.88.16 0.0.0.15 any
permit ip 104.211.98.138 0.0.0.0 any
permit ip 104.211.98.146 0.0.0.0 any
permit ip 104.211.98.246 0.0.0.0 any
permit ip 104.211.99.236 0.0.0.0 any
permit ip 104.211.100.160 0.0.0.0 any
permit ip 104.211.100.204 0.0.0.0 any
permit ip 104.211.102.225 0.0.0.0 any
permit ip 104.211.152.32 0.0.0.31 any
permit ip 104.211.161.150 0.0.0.0 any
permit ip 104.211.161.165 0.0.0.0 any
permit ip 104.211.161.185 0.0.0.0 any
permit ip 104.211.162.33 0.0.0.0 any
permit ip 104.211.165.35 0.0.0.0 any
permit ip 104.211.166.139 0.0.0.0 any
permit ip 104.211.216.32 0.0.0.31 any
permit ip 104.211.224.118 0.0.0.0 any
permit ip 104.211.225.135 0.0.0.0 any
permit ip 104.211.227.110 0.0.0.0 any
permit ip 104.211.231.147 0.0.0.0 any
permit ip 104.211.231.248 0.0.0.0 any
permit ip 104.215.96.24 0.0.0.7 any
permit ip 104.215.144.64 0.0.0.7 any
permit ip 104.215.184.16 0.0.0.7 any
permit ip 131.253.120.128 0.0.0.0 any
permit ip 132.245.165.0 0.0.0.127 any
permit ip 134.170.67.0 0.0.0.127 any
permit ip 134.170.172.128 0.0.0.127 any
permit ip 157.55.45.128 0.0.0.127 any
permit ip 157.55.59.128 0.0.0.127 any
permit ip 157.55.130.0 0.0.0.127 any
permit ip 157.55.145.0 0.0.0.127 any
permit ip 157.55.155.0 0.0.0.127 any
permit ip 157.55.227.192 0.0.0.63 any
permit ip 157.56.53.128 0.0.0.127 any
permit ip 157.56.55.0 0.0.0.127 any
permit ip 157.56.58.0 0.0.0.127 any
permit ip 157.56.151.0 0.0.0.127 any
permit ip 191.232.2.128 0.0.0.127 any
permit ip 191.237.248.32 0.0.0.7 any
permit ip 191.237.252.192 0.0.0.15 any
    </addresslist>
    <addresslist type="IPv6">
      <address>2603:1020:201::4a0/128</address>
      <address>2603:1020:201::4a1/128</address>
      <address>2603:1020:201::4a2/128</address>
      <address>2603:1020:201::4a3/128</address>
      <address>2603:1020:201::4a4/128</address>
      <address>2603:1020:201::4a5/128</address>
      <address>2603:1020:201::4a6/128</address>
      <address>2603:1020:201::4a7/128</address>
      <address>2603:1020:201::4aa/128</address>
      <address>2603:1020:201::581/128</address>
      <address>2603:1020:201::583/128</address>
      <address>2603:1020:201::584/128</address>
      <address>2603:1020:201::586/128</address>
      <address>2603:1020:201::588/128</address>
      <address>2603:1020:201::589/128</address>
      <address>2603:1020:201::58a/128</address>
      <address>2603:1020:201::58b/128</address>
      <address>2603:1020:201::58c/128</address>
      <address>2603:1020:201:2::/64</address>
      <address>2603:1020:201:3::/64</address>
      <address>2603:1030:7::2c/128</address>
      <address>2603:1030:7::2d/128</address>
      <address>2603:1030:7::2f/128</address>
      <address>2603:1030:7::30/128</address>
      <address>2603:1030:7::34/128</address>
      <address>2603:1030:7::3f/128</address>
      <address>2603:1030:7::40/128</address>
      <address>2603:1030:7::41/128</address>
      <address>2a01:111:2005:6::/64</address>
      <address>2a01:111:f100:1002::4134:d89f/128</address>
      <address>2a01:111:f100:1002::4134:d944/128</address>
      <address>2a01:111:f100:1002::4134:d95f/128</address>
      <address>2a01:111:f100:1002::4134:da55/128</address>
      <address>2a01:111:f100:1002::4134:da5c/128</address>
      <address>2a01:111:f100:1002::4134:da81/128</address>
      <address>2a01:111:f100:1002::4134:dab5/128</address>
      <address>2a01:111:f100:1002::4134:daee/128</address>
      <address>2a01:111:f100:1002::4134:db2a/128</address>
      <address>2a01:111:f100:1002::4134:db60/128</address>
      <address>2a01:111:f100:1002::4134:db89/128</address>
      <address>2a01:111:f100:1002::4134:dbe7/128</address>
      <address>2a01:111:f100:1002::4134:dc2d/128</address>
      <address>2a01:111:f100:1002::4134:dc2e/128</address>
      <address>2a01:111:f100:1002::4134:dc43/128</address>
      <address>2a01:111:f100:1002::4134:dc6e/128</address>
      <address>2a01:111:f100:1002::4134:dd7a/128</address>
      <address>2a01:111:f100:1002::4134:ddcb/128</address>
      <address>2a01:111:f100:2002::8975:2c3b/128</address>
      <address>2a01:111:f100:2002::8975:2c3f/128</address>
      <address>2a01:111:f100:2002::8975:2c6d/128</address>
      <address>2a01:111:f100:2002::8975:2cdd/128</address>
      <address>2a01:111:f100:2002::8975:2cea/128</address>
      <address>2a01:111:f100:2002::8975:2ced/128</address>
      <address>2a01:111:f100:2002::8975:2d08/128</address>
      <address>2a01:111:f100:2002::8975:2d19/128</address>
      <address>2a01:111:f100:2002::8975:2d25/128</address>
      <address>2a01:111:f100:2002::8975:2d4d/128</address>
      <address>2a01:111:f100:2002::8975:2d6a/128</address>
      <address>2a01:111:f100:2002::8975:2d97/128</address>
      <address>2a01:111:f100:2002::8975:2daa/128</address>
      <address>2a01:111:f100:2002::8975:2dc7/128</address>
      <address>2a01:111:f100:3002::8987:30a0/128</address>
      <address>2a01:111:f100:3002::8987:3103/128</address>
      <address>2a01:111:f100:3002::8987:3278/128</address>
      <address>2a01:111:f100:3002::8987:328f/128</address>
      <address>2a01:111:f100:3002::8987:3299/128</address>
      <address>2a01:111:f100:3002::8987:3344/128</address>
      <address>2a01:111:f100:3002::8987:3396/128</address>
      <address>2a01:111:f100:3002::8987:3398/128</address>
      <address>2a01:111:f100:3002::8987:33b3/128</address>
      <address>2a01:111:f100:3002::8987:33ec/128</address>
      <address>2a01:111:f100:3002::8987:34eb/128</address>
      <address>2a01:111:f100:3002::8987:34f8/128</address>
      <address>2a01:111:f100:3002::8987:353b/128</address>
      <address>2a01:111:f100:3002::8987:35b5/128</address>
      <address>2a01:111:f100:4001::4625:a3ee/128</address>
      <address>2a01:111:f100:4001::4625:a4b6/128</address>
      <address>2a01:111:f100:4001::4625:a4ba/128</address>
      <address>2a01:111:f100:4001::4625:a4c7/128</address>
      <address>2a01:111:f100:4001::4625:a4cf/128</address>
      <address>2a01:111:f100:4001::4625:a4ee/128</address>
      <address>2a01:111:f100:4001::4625:a56f/128</address>
      <address>2a01:111:f100:4001::4625:a589/128</address>
      <address>2a01:111:f100:7000::6fdd:6a44/128</address>
      <address>2a01:111:f100:7000::6fdd:6b96/128</address>
      <address>2a01:111:f100:7000::6fdd:6bb6/128</address>
      <address>2a01:111:f100:7000::6fdd:6c82/128</address>
      <address>2a01:111:f100:7000::6fdd:6d1c/128</address>
      <address>2a01:111:f100:7000::6fdd:6d23/128</address>
      <address>2a01:111:f100:7000::6fdd:6d50/128</address>
      <address>2a01:111:f100:7000::6fdd:6d88/128</address>
      <address>2a01:111:f100:a004::bfeb:8a92/128</address>
      <address>2a01:111:f100:a004::bfeb:8ab0/128</address>
      <address>2a01:111:f100:a004::bfeb:8b12/128</address>
      <address>2a01:111:f100:a004::bfeb:8b15/128</address>
      <address>2a01:111:f100:a004::bfeb:8b3c/128</address>
      <address>2a01:111:f100:a004::bfeb:8b47/128</address>
      <address>2a01:111:f100:a004::bfeb:8b6c/128</address>
      <address>2a01:111:f100:a004::bfeb:8beb/128</address>
      <address>2a01:111:f100:a004::bfeb:8c55/128</address>
      <address>2a01:111:f100:a004::bfeb:8c6d/128</address>
      <address>2a01:111:f100:a004::bfeb:8c6f/128</address>
      <address>2a01:111:f100:a004::bfeb:8c88/128</address>
      <address>2a01:111:f100:a004::bfeb:8cc0/128</address>
      <address>2a01:111:f100:a004::bfeb:8cdc/128</address>
      <address>2a01:111:f100:a004::bfeb:8d83/128</address>
      <address>2a01:111:f100:a004::bfeb:8d96/128</address>
      <address>2a01:111:f100:a004::bfeb:8daa/128</address>
      <address>2a01:111:f102:8001::1761:4929/128</address>
      <address>2a01:111:f102:8001::1761:4948/128</address>
      <address>2a01:111:f102:8001::1761:4b83/128</address>
      <address>2a01:111:f102:8001::1761:4f0d/128</address>
      <address>2a01:111:f102:8001::1761:4f32/128</address>
      <address>2a01:111:f102:8001::1761:4f64/128</address>
      <address>2a01:111:f102:8001::1761:4f8d/128</address>
      <address>2a01:111:f102:8001::1761:4fc0/128</address>
      <address>2a01:111:f400::/48</address>
      <address>2001:df0:d9:200::/64</address>
      <address>2603:1047:100::/64</address>
      <address>2a01:111:2035:8::/64</address>
      <address>2a01:111:200a:a::/64</address>
      <address>2a01:111:f406:1::/64</address>
      <address>2a01:111:f406:2::/64</address>
      <address>2a01:111:f406:1004::/64</address>
      <address>2a01:111:f406:1805::/64</address>
      <address>2a01:111:f406:3404::/64</address>
      <address>2A01:111:F406:8000::/64</address>
      <address>2a01:111:f406:8801::/64</address>
      <address>2a01:111:f406:a003::/64</address>
      <address>2A01:111:F406:C00::/64</address>
    </addresslist>
  </product>

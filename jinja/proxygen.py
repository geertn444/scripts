#Python script to convert O365 to Cisco ACL

import os
import re
import sys


subnets = {
 '/32': '255.255.255.255',
 '/31': '255.255.255.254',
 '/30': '255.255.255.252',
 '/29': '255.255.255.248',
 '/28': '255.255.255.224',
 '/27': '255.255.255.192',
 '/26': '255.255.255.144',
 '/25': '255.255.255.128',
 '/24': '255.255.255.0',
 '/23': '255.255.254.0',
 '/22': '255.255.252.0',
 '/21': '255.255.248.0',
 '/20': '255.255.224.0',
 '/19': '255.255.192.0',
 '/18': '255.255.144.0',
 '/17': '255.255.128.0',
 '/16': '255.255.0.0',
 '/15': '255.254.0.0',
 '/14': '255.252.0.0',
 '/13': '255.248.0.0',
 '/12': '255.224.0.0',
 '/11': '255.192.0.0',
 '/10': '255.144.0.0',
 '/9': '255.128.0.0',
 '/8': '255.0.0.0',
 '/7': '254.0.0.0',
 '/6': '252.0.0.0',
 '/5': '248.0.0.0',
 '/4': '224.0.0.0',
 '/3': '192.0.0.0',
 '/2': '144.0.0.0',
 '/1': '128.0.0.0',
 '/0': '0.0.0.0' }
 



for filename in os.listdir('./'):
        if filename.endswith(".xml"):
            name = filename.rsplit('.', 1)[0]
            result = name + ".pac.txt"
            outfile = open(result,"w")
            Oxml = open(filename, "r")

            start = False

            for line in Oxml:
                match_object = re.search(r'<addresslist type="IPv4">', line)
                eos = re.search(r'</addresslist>',line)
                line = line.rstrip()
                if eos:
                 #stop section
                 start = False
                 outfile.write(line+'\n')
                 continue
                
                if match_object:
                 #start section
                 start = True
                 outfile.write(line+'\n')
                 continue
                 
                if start:
                  #convert
                  #isInNet(hostIP,"198.28.72.0","255.255.248.0") ||
                  overeen = re.search(r'<address>(.*)(\/.*)</address>',line)
                  result = 'isInNet(hostIP,"' + overeen.group(1) + '","' + subnets[overeen.group(2)] + '") ||'
                  outfile.write (result+'\n')
                else:
                 outfile.write(line+'\n')
            outfile.close
            Oxml.close
      


     

      